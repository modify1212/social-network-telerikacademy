create schema socialnetwork collate latin1_swedish_ci;

create table contact_infos
(
	contact_info_id int auto_increment
		primary key,
	email varchar(50) null,
	mobile varchar(50) null,
	privacy int not null
);

create table countries
(
	country_id int auto_increment
		primary key,
	name varchar(50) not null
);

create table post_types
(
	post_type_id int auto_increment
		primary key,
	post_type varchar(50) not null,
	constraint post_types_post_type_uindex
		unique (post_type)
);

create table teams
(
	team_id int auto_increment
		primary key,
	name varchar(50) not null,
	enabled tinyint not null
);

create table users
(
	user_id int auto_increment
		primary key,
	username varchar(50) not null,
	password varchar(68) not null,
	enabled tinyint not null,
	first_name varchar(50) not null,
	last_name varchar(50) not null,
	country int default 247 not null,
	attitude varchar(50) null,
	team_id int default 1 not null,
	picture longblob null,
	background_picture longblob null,
	birth_year varchar(50) null,
	contact_info_id int not null,
	constraint users_username_uindex
		unique (username),
	constraint FK7uplwiv7y3gj5wn5guyj4nu1y
		foreign key (country) references countries (country_id),
	constraint users_contact_infos_contact_info_id_fk
		foreign key (contact_info_id) references contact_infos (contact_info_id),
	constraint users_teams_team_id_fk
		foreign key (team_id) references teams (team_id)
);

create table authorities
(
	authority varchar(50) not null,
	username varchar(50) not null,
	constraint authorities_fk0
		foreign key (username) references users (username)
);

create table friend_requests
(
	friend_request_id int auto_increment
		primary key,
	sender int not null,
	recipient int not null,
	constraint friend_requests_users_user_id_fk
		foreign key (sender) references users (user_id),
	constraint friend_requests_users_user_id_fk_2
		foreign key (recipient) references users (user_id)
);

create table friends
(
	friendship_id int auto_increment
		primary key,
	friend_one int not null,
	friend_two int not null,
	constraint friends_users_user_id_fk
		foreign key (friend_one) references users (user_id),
	constraint friends_users_user_id_fk_2
		foreign key (friend_two) references users (user_id)
);

create table posts
(
	post_id int auto_increment
		primary key,
	description varchar(200) not null,
	is_public tinyint not null,
	enabled tinyint not null,
	user_id int not null,
	post_type int not null,
	picture longblob null,
	created_at datetime not null,
	updated_at datetime not null,
	popularity int null,
	constraint posts_post_types_post_type_id_fk
		foreign key (post_type) references post_types (post_type_id),
	constraint posts_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

create table comments
(
	comment_id int auto_increment
		primary key,
	comment varchar(500) not null,
	post_id int not null,
	user_id int not null,
	created_at datetime not null,
	updated_at datetime not null,
	constraint comments_posts_post_id_fk
		foreign key (post_id) references posts (post_id),
	constraint comments_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

create table likes
(
	like_id int auto_increment
		primary key,
	post_id int not null,
	user_id int not null,
	constraint likes_posts_post_id_fk
		foreign key (post_id) references posts (post_id),
	constraint likes_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

