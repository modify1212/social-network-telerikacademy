# War Forum - Social Network
*by Stayko Tzenov & Rosen Neshevski*   
*Team: A17-Team-15*  

[Inital GitLab repo](https://gitlab.com/modify1212/social-network-telerikacademy)

Link to the team [Trello](https://trello.com/b/EtbUx8IB/social-network)

It is currently hosted and you can go and check it out:

https://warforum.tzenovs.com/
                    
                   
                    
## How To Run if the hosted site is currently down 
To run the application you should:
1. Clone the project Git repository.
2. Run the DB script to create tables.
3. Edit the DB url, username and password fields in application.properties file according to your configuration.  
4. Edit paths for storing files in the application.properties file.
6. Build and run the applicataion

The application will be available [here](http://localhost:8080).

The REST part of the appliactaion is documented with Swagger and is available [here](http://localhost:8080/swagger-ui.html) while the application is running.

                  
                  

## Sample Screenshots

### User login page
![Login page](Login1.jpg)

### User register page
![Register page](Register.jpg)

### User Profile page
![Profile page](Profile.jpg)

### Neews Feed page
![Neews Feed page](Neews Feed.jpg)

### Admin page
![Admin page](Admin.jpg)

### Update User Profile page
![Update Profile page](Update Profile.jpg)