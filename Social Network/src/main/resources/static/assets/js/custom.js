/*
Template: SocialV - Responsive Bootstrap 4 Admin Dashboard Template
Author: iqonicthemes.in
Design and Developed by: iqonicthemes.in
NOTE: This file contains the styling for responsive Template.
*/

/*----------------------------------------------
Index Of Script
------------------------------------------------

:: Tooltip
:: Sidebar Widget
:: Magnific Popup
:: Ripple Effect
:: Page faq
:: Page Loader
:: Owl Carousel
:: Select input
:: Search input
:: Scrollbar
:: Counter
:: slick
:: Progress Bar
:: Page Menu
:: Page Loader
:: Wow Animation
:: Mail Inbox
:: Chat
:: Todo
:: Form Validation
:: Sidebar Widget
:: Flatpicker

------------------------------------------------
Index Of Script
----------------------------------------------*/

(function (jQuery) {


    "use strict";

    jQuery(document).ready(function () {

        /*---------------------------------------------------------------------
        Tooltip
        -----------------------------------------------------------------------*/
        jQuery('[data-toggle="popover"]').popover();
        jQuery('[data-toggle="tooltip"]').tooltip();


        /*---------------------------------------------------------------------
        Magnific Popup
        -----------------------------------------------------------------------*/
        jQuery('.popup-gallery').magnificPopup({
            delegate: 'a.popup-img',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });
        jQuery('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });


        /*---------------------------------------------------------------------
        Ripple Effect
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', ".iq-waves-effect", function (e) {
            // Remove any old one
            jQuery('.ripple').remove();
            // Setup
            let posX = jQuery(this).offset().left,
                posY = jQuery(this).offset().top,
                buttonWidth = jQuery(this).width(),
                buttonHeight = jQuery(this).height();

            // Add the element
            jQuery(this).prepend("<span class='ripple'></span>");


            // Make it round!
            if (buttonWidth >= buttonHeight) {
                buttonHeight = buttonWidth;
            } else {
                buttonWidth = buttonHeight;
            }

            // Get the center of the element
            let x = e.pageX - posX - buttonWidth / 2;
            let y = e.pageY - posY - buttonHeight / 2;


            // Add the ripples CSS and start the animation
            jQuery(".ripple").css({
                width: buttonWidth,
                height: buttonHeight,
                top: y + 'px',
                left: x + 'px'
            }).addClass("rippleEffect");
        });

        /*---------------------------------------------------------------------
        Page faq
        -----------------------------------------------------------------------*/
        jQuery('.iq-accordion .iq-accordion-block .accordion-details').hide();
        jQuery('.iq-accordion .iq-accordion-block:first').addClass('accordion-active').children().slideDown('slow');
        jQuery(document).on("click", '.iq-accordion .iq-accordion-block', function () {
            if (jQuery(this).children('div.accordion-details ').is(':hidden')) {
                jQuery('.iq-accordion .iq-accordion-block').removeClass('accordion-active').children('div.accordion-details ').slideUp('slow');
                jQuery(this).toggleClass('accordion-active').children('div.accordion-details ').slideDown('slow');
            }
        });

        /*---------------------------------------------------------------------
        Page Loader
        -----------------------------------------------------------------------*/
        jQuery("#load").fadeOut();
        jQuery("#loading").delay().fadeOut("");


        /*---------------------------------------------------------------------
       Owl Carousel
       -----------------------------------------------------------------------*/
        jQuery('.owl-carousel').each(function () {
            let jQuerycarousel = jQuery(this);
            jQuerycarousel.owlCarousel({
                items: jQuerycarousel.data("items"),
                loop: jQuerycarousel.data("loop"),
                margin: jQuerycarousel.data("margin"),
                nav: jQuerycarousel.data("nav"),
                dots: jQuerycarousel.data("dots"),
                autoplay: jQuerycarousel.data("autoplay"),
                autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
                navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
                responsiveClass: true,
                responsive: {
                    // breakpoint from 0 up
                    0: {
                        items: jQuerycarousel.data("items-mobile-sm"),
                        nav: false,
                        dots: true
                    },
                    // breakpoint from 480 up
                    480: {
                        items: jQuerycarousel.data("items-mobile"),
                        nav: false,
                        dots: true
                    },
                    // breakpoint from 786 up
                    786: {
                        items: jQuerycarousel.data("items-tab")
                    },
                    // breakpoint from 1023 up
                    1023: {
                        items: jQuerycarousel.data("items-laptop")
                    },
                    1199: {
                        items: jQuerycarousel.data("items")
                    }
                }
            });
        });

        /*---------------------------------------------------------------------
        Select input
        -----------------------------------------------------------------------*/
        jQuery('.select2jsMultiSelect').select2({
            tags: true
        });

        /*---------------------------------------------------------------------
        Search input
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', function (e) {
            let myTargetElement = e.target;
            let selector, mainElement;
            if (jQuery(myTargetElement).hasClass('search-toggle') || jQuery(myTargetElement).parent().hasClass('search-toggle') || jQuery(myTargetElement).parent().parent().hasClass('search-toggle')) {
                if (jQuery(myTargetElement).hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent();
                    mainElement = jQuery(myTargetElement);
                } else if (jQuery(myTargetElement).parent().hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent().parent();
                    mainElement = jQuery(myTargetElement).parent();
                } else if (jQuery(myTargetElement).parent().parent().hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent().parent().parent();
                    mainElement = jQuery(myTargetElement).parent().parent();
                }
                if (!mainElement.hasClass('active') && jQuery(".navbar-list li").find('.active')) {
                    jQuery('.navbar-list li').removeClass('iq-show');
                    jQuery('.navbar-list li .search-toggle').removeClass('active');
                }

                selector.toggleClass('iq-show');
                mainElement.toggleClass('active');

                e.preventDefault();
            } else if (jQuery(myTargetElement).is('.search-input')) {
            } else {
                jQuery('.navbar-list li').removeClass('iq-show');
                jQuery('.navbar-list li .search-toggle').removeClass('active');
            }
        });

        /*---------------------------------------------------------------------
        Scrollbar
        -----------------------------------------------------------------------*/
        let Scrollbar = window.Scrollbar;
        if (jQuery('#sidebar-scrollbar').length) {
            Scrollbar.init(document.querySelector('#sidebar-scrollbar'), options);
        }
        let Scrollbar1 = window.Scrollbar;
        if (jQuery('#right-sidebar-scrollbar').length) {
            Scrollbar1.init(document.querySelector('#right-sidebar-scrollbar'), options);
        }


        /*---------------------------------------------------------------------
        Counter
        -----------------------------------------------------------------------*/
        jQuery('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        /*---------------------------------------------------------------------
        slick
        -----------------------------------------------------------------------*/
        jQuery('.slick-slider').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 9,
            slidesToScroll: 1,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30',
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '15',
                    slidesToShow: 1
                }
            }],
            nextArrow: '<a href="#" class="ri-arrow-left-s-line left"></a>',
            prevArrow: '<a href="#" class="ri-arrow-right-s-line right"></a>',
        });

        jQuery('#new-music').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            focusOnSelect: true,
            arrows: false,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 1
                }
            }],

        });

        jQuery('#recent-music').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            focusOnSelect: true,
            arrows: false,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 1
                }
            }],

        });

        jQuery('#top-music').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            focusOnSelect: true,
            arrows: false,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    slidesToShow: 1
                }
            }],

        });


        /*---------------------------------------------------------------------
        Progress Bar
        -----------------------------------------------------------------------*/
        jQuery('.iq-progress-bar > span').each(function () {
            let progressBar = jQuery(this);
            let width = jQuery(this).data('percent');
            progressBar.css({
                'transition': 'width 2s'
            });

            setTimeout(function () {
                progressBar.appear(function () {
                    progressBar.css('width', width + '%');
                });
            }, 100);
        });


        /*---------------------------------------------------------------------
        Page Menu
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.wrapper-menu', function () {
            jQuery(this).toggleClass('open');
        });

        jQuery(document).on('click', ".wrapper-menu", function () {
            jQuery("body").toggleClass("sidebar-main");
        });


        /*---------------------------------------------------------------------
        Wow Animation
        -----------------------------------------------------------------------*/
        let wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: false,
            live: true
        });
        wow.init();


        /*---------------------------------------------------------------------
        Mailbox
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', 'ul.iq-email-sender-list li', function () {
            jQuery(this).next().addClass('show');
        });

        jQuery(document).on('click', '.email-app-details li h4', function () {
            jQuery('.email-app-details').removeClass('show');
        });


        /*---------------------------------------------------------------------
        chatuser
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.chat-head .chat-user-profile', function () {
            jQuery(this).parent().next().toggleClass('show');
        });
        jQuery(document).on('click', '.user-profile .close-popup', function () {
            jQuery(this).parent().parent().removeClass('show');
        });

        /*---------------------------------------------------------------------
        chatuser main
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.chat-search .chat-profile', function () {
            jQuery(this).parent().next().toggleClass('show');
        });
        jQuery(document).on('click', '.user-profile .close-popup', function () {
            jQuery(this).parent().parent().removeClass('show');
        });

        /*---------------------------------------------------------------------
        Chat start
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '#chat-start', function () {
            jQuery('.chat-data-left').toggleClass('show');
        });
        jQuery(document).on('click', '.close-btn-res', function () {
            jQuery('.chat-data-left').removeClass('show');
        });
        jQuery(document).on('click', '.iq-chat-ui li', function () {
            jQuery('.chat-data-left').removeClass('show');
        });
        jQuery(document).on('click', '.sidebar-toggle', function () {
            jQuery('.chat-data-left').addClass('show');
        });

        /*---------------------------------------------------------------------
        todo Page
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.todo-task-list > li > a', function () {
            jQuery('.todo-task-list li').removeClass('active');
            jQuery('.todo-task-list .sub-task').removeClass('show');
            jQuery(this).parent().toggleClass('active');
            jQuery(this).next().toggleClass('show');
        });
        jQuery(document).on('click', '.todo-task-list > li li > a', function () {
            jQuery('.todo-task-list li li').removeClass('active');
            jQuery(this).parent().toggleClass('active');
        });
        /*---------------------------------------------------------------------
        Form Validation
        -----------------------------------------------------------------------*/

        // Example starter JavaScript for disabling form submissions if there are invalid fields
        window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);

        /*---------------------------------------------------------------------
        Sidebar Widget
        -----------------------------------------------------------------------*/
        jQuery(document).ready(function () {
            jQuery().on('click', '.todo-task-lists li', function () {
                if (jQuery(this).find('input:checkbox[name=todo-check]').is(":checked")) {

                    jQuery(this).find('input:checkbox[name=todo-check]').attr("checked", false);
                    jQuery(this).removeClass('active-task');
                } else {
                    jQuery(this).find('input:checkbox[name=todo-check]').attr("checked", true);
                    jQuery(this).addClass('active-task');
                }

            });
        });


        /*------------------------------------------------------------------
        Flatpicker
        * -----------------------------------------------------------------*/
        if (typeof flatpickr !== 'undefined' && jQuery.isFunction(flatpickr)) {
            jQuery(".flatpicker").flatpickr({
                inline: true
            });
        }


    });
})(jQuery);


(function postComment() {

    $(document).on("click", ".add-comment-btn", (ev) => {
        let form = $(ev.currentTarget).parent().parent();

        let comment = form.find(".commentText").val();
        let postId = form.find(".postId").val();
        let principalId = form.find(".principalId").val();
        let totalComments = form.parent().siblings(".show-comment");
        let newTotalComments = parseInt(totalComments.children(".comment-count").text(), 10) + 1;

        data = {
            postId,
            principalId,
            comment
        };

        $.ajax({
            type: 'POST',
            url: "https://warforum.tzenovs.com/comments/post-comment",
            data: data,
            contentType: "application/x-www-form-urlencoded",
            error: function () {
                alert("Maximum comments per post have been reached");
            },
            success: function (response) {

                form.parent().prev().append(response);
                totalComments.replaceWith(`<a role="button" href="javascript:void();" class="show-comment">
            <span  class="comment-count">${newTotalComments}</span>
            <span> Comments</span>
        </a>`)
                comment = '';
            }
        });
    });
})();


(function deleteComment() {

    $(document).on("click", ".delete-comment-btn", (ev) => {

        let form = $(ev.currentTarget).parent();
        let commentId = form.find(".commentId").val();
        let totalComments = form.parent().parent().parent().parent().parent().parent().find(".show-comment");
        let newTotalComments = parseInt(totalComments.children(".comment-count").text(), 10) - 1;
        data = {
            commentId
        };

        $.ajax({
            type: 'DELETE',
            url: `https://warforum.tzenovs.com/api/comments/${commentId}`,
            data: data,
            contentType: "application/x-www-form-urlencoded",
            error: function () {
                alert("Error");
            },
            success: function () {
                form.parent().parent().remove();
                totalComments.replaceWith(`<a role="button" href="javascript:void();" class="show-comment">
            <span  class="comment-count">${newTotalComments}</span>
            <span> Comments</span>
        </a>`)

            }
        });
    });
})();


(function deletePost() {

    $(document).on("click", ".delete-post-btn", (ev) => {

        let form = $(ev.currentTarget).parent();
        let data = form.find(".postId").val();
        let url = `https://warforum.tzenovs.com/api/posts/${data}`;

        $.ajax({
            type: 'DELETE',
            url: url,
            data: data,
            contentType: "application/x-www-form-urlencoded",
            error: function () {
                alert("WTF");
            },
            success: function () {

                form.parentsUntil(".post-subfragment").parent().remove();
            }
        });
    });
})();





// LIKE BUTTON -----------------------------------------------------------
(function like(){
    $(document).on("click", ".like-button", (ev) =>{
        let form = $(ev.currentTarget).parent();
        let postId = form.find(".postId").val();
        let userId = form.find(".userId").val();
        let totalLikes = form.find(".totalLikes").val();
        let totalLikesInputTag = form.find(".totalLikes");
        let url = `https://warforum.tzenovs.com/api/likes/like-post?userId=${userId}&postId=${postId}`;

        data = {
            postId,
            userId
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            contentType: "application/x-www-form-urlencoded",
            error: function () {
                alert("Something went wrong with this like");
            },
            success: function (response) {
                form.parent()
                    .parent()
                    .siblings(".total-like-block")
                    .children()
                    .find(".dropdown-toggle")
                    .replaceWith(`<span class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
                                   aria-expanded="false" role="button">
                             ${response} Likes
                             </span>`)
                if(parseInt(response, 10) < parseInt(totalLikes, 10)){
                    form.find(".like-button").replaceWith(`<span class="dropdown-toggle like-button"  aria-haspopup="true"
                                   aria-expanded="false" role="button">
                                <img  src="/assets/images/icon/00.png" class="img-fluid" alt="">
                                </span>`)
                }else {
                    form.find(".like-button").replaceWith(`<span class="dropdown-toggle like-button"  aria-haspopup="true"
                                   aria-expanded="false" role="button">
                                <img  src="/assets/images/icon/01.png" class="img-fluid" alt="">
                                </span>`)
                }

                totalLikesInputTag.replaceWith(`<input type="hidden" class="totalLikes" value="${response}">`)
            }
        })
    })
})();
//---------------------------------------------------------------------------

// SHOW COMMENTS TOGGLE BUTTON-----------------------------------------------
(function showComments() {
    $(document).on("click", ".show-comment", (ev) => {
        let button = $(ev.currentTarget);
        let comments = button.siblings(".post-comments-fragment").toggle(500);
    })

})();
//---------------------------------------------------------------------------


    var globalFilter;
// FILTER NEWSFEED ----------------------------------------------------------
(function filterNewsfeed() {
    $(document).on("click", ".filter-button", (ev) => {
        let button = $(ev.currentTarget);
        let form = button.parent();
        let filter = button.find(".filter-input").val();
        globalFilter = filter;
        let url = `https://warforum.tzenovs.com/`;

        data = {
            filter,
        };

        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            contentType: "application/x-www-form-urlencoded",
            error: function(){
                alert("No posts of this type available");
            },
            success: function (response) {
                form.parent()
                    .parent()
                    .parent()
                    .find(".post-fragment")
                    .replaceWith(response);

                button.attr("class", "filter-button active");
                button.siblings(".filter-button").attr('class', 'filter-button');
            }
        })
    })
})();
//---------------------------------------------------------------------------
// LOAD NEXT PAGE -----------------------------------------------------------
(function loadNextPage() {

    $(document).on("click", ".load-next-page-btn", (ev) => {
        let form = $(ev.currentTarget).parent();
        let pageNumber = form.find(".pageNumber").val();
        let url = `https://warforum.tzenovs.com/load-next-page?pageNumber=${pageNumber}&filter=${globalFilter}`;



        $.ajax({
            type: 'GET',
            url: url,

            contentType: "application/x-www-form-urlencoded",
            error: function () {
                alert("No more posts to load");

            },
            success: function (response) {

                form.prev().append(response);
                form.remove();
            }
        });
    });
})();

//---------------------------------------------------------------------------
(function search() {

    $(document).on("keyup", "input.search-input", (ev)=>{
        let search = $(ev.currentTarget).val();
        if(search.length < 3){
            $("#search-results").fadeOut();
        }if(search.length >= 3){
            $("#search-results").fadeIn();

            data ={
                search
            };
            $.ajax({
                type: 'GET',
                url: "https://warforum.tzenovs.com/users/search",
                contentType: "application/x-www-form-urlencoded",
                data: data,
                success: function (response) {
                    $("#search-results").children().replaceWith(response);
                }
            })
        }

    })
})();