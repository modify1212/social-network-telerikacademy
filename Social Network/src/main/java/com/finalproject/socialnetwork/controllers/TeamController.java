package com.finalproject.socialnetwork.controllers;

import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.models.TeamRequestChecker;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

@Controller
@RequestMapping("/teams")
public class TeamController {

    private TeamService teamService;
    private TeamRequestService teamRequestService;
    private UserService userService;
    private FriendRequestService friendRequestService;
    private FriendshipService friendshipService;
    private ImageService imageService;

    public TeamController(TeamService teamService,
                          TeamRequestService teamRequestService,
                          UserService userService,
                          FriendRequestService friendRequestService,
                          ImageService imageService,
                          FriendshipService friendshipService) {
        this.teamService = teamService;
        this.teamRequestService = teamRequestService;
        this.userService = userService;
        this.friendRequestService = friendRequestService;
        this.imageService = imageService;
        this.friendshipService = friendshipService;
    }

    @GetMapping
    public String showTeams(Principal principal, Model model) {
        if (principal != null) {
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
        }
        model.addAttribute("teams", teamService.getAll());
        model.addAttribute("requestChecker", new TeamRequestChecker(teamRequestService));
        return "group";
    }


    @PostMapping("/create")
    public String create(Principal principal) {

        if (principal == null) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Please Log In");
        }

        User user = userService.getByUsername(principal.getName());

        Team team = new Team();
        team.setCreator(user);
        team.setName("New Team");
        team.setDescription("New Team");
        team.setAttitude(5);
        team.setEnabled(1);
        teamService.create(team);
        user.setTeam(team);
        return String.format("redirect:/teams/%d", team.getId());
    }

    @GetMapping("/{id}/update")
    public String updateForm(@PathVariable(name = "id") int teamId, Model model, Principal principal) {
        if (principal != null) {
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
        }

        model.addAttribute("team", teamService.getById(teamId));
        return "team-update";
    }

    @PostMapping("/{id}/update")
    public String update(@PathVariable(name = "id") int teamId,
                         @RequestParam("teamName") String teamName,
                         @RequestParam("description") String description,
                         @RequestParam("attitude") int attitude,
                         @RequestParam("imagefile") MultipartFile file) throws IOException {

        Team team = teamService.getById(teamId);
        team.setName(teamName);
        team.setDescription(description);
        team.setAttitude(attitude);

        if (!file.isEmpty()) {
            imageService.saveTeamImage(team, file);
        } else {
            teamService.update(team);
        }
        return "redirect:/teams/{id}";
    }

    @GetMapping("/{id}")
    public String teamProfile(@PathVariable(name = "id") int teamId, Model model, Principal principal) {

        if (principal != null) {
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
            model.addAttribute("principalFriends", friendshipService.getFriendsByUserId(principalUser.getId()));

        }
        model.addAttribute("requestChecker", new TeamRequestChecker(teamRequestService));
        model.addAttribute("team", teamService.getById(teamId));
        model.addAttribute("teamRequests", teamRequestService.getByTeam(teamId));

        return "team-page";
    }

    @PostMapping("/{id}/update/background")
    public String uploadBackgroundImage(@PathVariable int id, @RequestParam("imagefile") MultipartFile file) throws IOException {

        imageService.saveTeamBackgroundImage(id, file);

        return "redirect:/teams/{id}";
    }

    @PostMapping("/{id}/accept-request")
    public String acceptRequest(@PathVariable(name = "id") int teamId, @RequestParam("requestId") int requestId) {

        teamRequestService.accept(requestId);
        return "redirect:/teams/{id}";
    }

    @PostMapping("/{id}/send-request")
    public String sendRequest(@PathVariable(name = "id") int teamId, @RequestParam("userId") int userId) {
        teamRequestService.send(teamId, userId);
        return "redirect:/teams";
    }

    @PostMapping("/{id}/reject-request")
    public String rejectRequest(@PathVariable(name = "id") int teamId, @RequestParam("requestId") int requestId) {
        teamRequestService.reject(requestId);
        return "redirect:/teams/{id}";
    }

    @PostMapping("/{id}/leave-team")
    public String leaveTeam(@PathVariable(name = "id") int teamId, @RequestParam("userId") int userId) {
        userService.leaveTeam(userId);
        return "redirect:/teams";
    }

    @PostMapping("/{id}/delete-team")
    public String deleteTeam(@PathVariable(name = "id") int teamId){

        teamService.softDelete(teamService.getById(teamId));
        return "redirect:/teams";
    }
    //
//    @PostMapping("/{id}/update/picture")
//    public String uploadPicture(){
//
//    }
//
    @GetMapping("/{id}/picture")
    public void renderProfilePicture(@PathVariable int id, HttpServletResponse response) throws IOException {
        Team team = teamService.getById(id);

        if (team.getPicture() != null) {
            byte[] byteArray = new byte[team.getPicture().length];
            int i = 0;

            for (Byte wrappedByte : team.getPicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

    @GetMapping("/{id}/background")
    public void renderBackgroundImage(@PathVariable int id, HttpServletResponse response) throws IOException {
        Team team = teamService.getById(id);

        if (team.getBackgroundPicture() != null) {
            byte[] byteArray = new byte[team.getBackgroundPicture().length];
            int i = 0;

            for (Byte wrappedByte : team.getBackgroundPicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }
}
