package com.finalproject.socialnetwork.controllers;


import com.finalproject.socialnetwork.exceptions.AccessForbiddenException;
import com.finalproject.socialnetwork.exceptions.NotAuthorizedException;
import com.finalproject.socialnetwork.models.AuthorizationValidator;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.FriendRequestService;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

@Controller
public class AdminController {
    private UserService userService;
    private FriendRequestService friendRequestService;

    @Autowired
    public AdminController(UserService userService, FriendRequestService friendRequestService) {
        this.userService = userService;
        this.friendRequestService=friendRequestService;
    }

    @GetMapping("/admin")
    public String admin (){
        return "redirect:/admin/users-details";
    }

    @GetMapping("/admin/users-details")
    public String showUsers(Model model,
                            @ModelAttribute(name = "userSearch")User user,
                            Principal principal){

        if (AuthorizationValidator.isAdmin()){
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
           }else {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,"You are not an admin");
        }
        model.addAttribute("users", userService.getAll());
        return "table-editable";
    }

    @ExceptionHandler(AccessForbiddenException.class)
    @ResponseStatus(value=HttpStatus.UNAUTHORIZED)
    private String handleForbiddenException() {
        return "error-403";
    }

}
