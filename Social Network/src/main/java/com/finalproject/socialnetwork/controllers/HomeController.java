package com.finalproject.socialnetwork.controllers;


import com.finalproject.socialnetwork.models.FriendRequest;
import com.finalproject.socialnetwork.models.LikeChecker;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.FriendRequestService;
import com.finalproject.socialnetwork.services.LikeService;
import com.finalproject.socialnetwork.services.PostService;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

@Controller
@RequestMapping("/")
public class HomeController {

    private UserService userService;
    private PostService postService;
    private FriendRequestService friendRequestService;
    private LikeService likeService;

    public HomeController(UserService userService, PostService postService, FriendRequestService friendRequestService, LikeService likeService) {
        this.userService = userService;
        this.postService = postService;
        this.friendRequestService = friendRequestService;
        this.likeService = likeService;
    }

    @GetMapping
    public String showHomePage(@RequestParam(name = "filter", required = false) String filter,
                               Principal principal,
                               Model model){

        if (principal != null) {
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
        }
        model.addAttribute("likeChecker", new LikeChecker(likeService));


        int page = 0;
        int size = 10;
        Pageable pageable = PageRequest.of(page,size);

        if(filter == null){
            model.addAttribute("userPosts", postService.getFeed(pageable));
        } else {
            Page<Post> filteredPosts = null;
            try {
                filteredPosts = postService.getByTypeId(pageable, Integer.parseInt(filter));
            } catch (EntityNotFoundException e) {
               throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
            model.addAttribute("userPosts",filteredPosts) ;
            return "fragments::post-fragment";
        }

        return "index";
    }

    @GetMapping("load-next-page")
    public ModelAndView loadNextPage(@RequestParam(name = "pageNumber") int pageNumber,
                                     @RequestParam(name = "filter", required = false)String filter,
                                     Model model,
                                     Principal principal){
        if(principal != null){
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser );
        }

        Page<Post> posts;
        int size = 10;
        Pageable pageable = PageRequest.of(pageNumber, size);

        if(filter == null || filter.equals("undefined")) {
            posts = postService.getFeed(PageRequest.of(pageNumber, size));
        } else {

            try {
                posts = postService.getByTypeId(pageable, Integer.parseInt(filter));
            } catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }
        model.addAttribute("userPosts", posts);
        model.addAttribute("likeChecker", new LikeChecker(likeService));


        if(posts.getPageable().getPageNumber() == posts.getTotalPages()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return new ModelAndView("fragments::post-fragment");
    }

    @GetMapping("/terms-and-conditions")
    public String showTermsAndConditions(Principal principal, Model model){
        if(principal != null){
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser );
        }
        return "terms-of-service";
    }
}
