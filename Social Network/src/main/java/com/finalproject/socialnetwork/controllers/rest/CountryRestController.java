package com.finalproject.socialnetwork.controllers.rest;

import com.finalproject.socialnetwork.models.Country;
import com.finalproject.socialnetwork.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {

    private CountryService countryService;

    @Autowired
    public CountryRestController (CountryService countryService){
        this.countryService=countryService;
    }

    @GetMapping
    public List<Country>getAll(){
            return countryService.getAll();
    }
    @GetMapping("/{id}")
    public Country getById(@PathVariable int id) {
        try {
            return countryService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
