package com.finalproject.socialnetwork.controllers.rest;


import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.services.CommentService;
import com.finalproject.socialnetwork.services.PostService;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/comments")
public class CommentRestController {

    private CommentService commentService;
    private UserService userService;
    private PostService postService;

    @Autowired
    public CommentRestController(CommentService commentService, UserService userService, PostService postService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
    }


    @RequestMapping(value = "/post-comment", method = RequestMethod.POST)
    public void postComment(@RequestParam(name = "postId") int postId,
                            @RequestParam(name = "comment") String commentMessage,
                            @RequestParam(name = "principalId") int principalId) {

        Comment comment = new Comment();
        comment.setMessage(commentMessage);
        comment.setUser(userService.getById(principalId));
        comment.setPost(postService.getById(postId));
        commentService.create(comment);
        postService.updatePopularity(postId);
    }

    @ResponseStatus(value = HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deleteById(@PathVariable int id){
        try {
            Comment comment = commentService.getById(id);
            commentService.delete(comment);
            postService.updatePopularity(comment.getPost().getId());
        }catch (EntityNotFoundException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
