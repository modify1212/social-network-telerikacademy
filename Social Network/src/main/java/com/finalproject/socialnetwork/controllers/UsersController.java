package com.finalproject.socialnetwork.controllers;


import com.finalproject.socialnetwork.exceptions.PasswordMismatchException;
import com.finalproject.socialnetwork.exceptions.WrongPasswordException;
import com.finalproject.socialnetwork.models.LikeChecker;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.models.dtos.UserPasswordChangeDTO;
import com.finalproject.socialnetwork.services.*;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UsersController {

    private UserService userService;
    private CountryService countryService;
    private ImageService imageService;
    private FriendshipService friendshipService;
    private FriendRequestService friendRequestService;
    private PostTypeService postTypeService;
    private PostService postService;
    private LikeService likeService;


    @Autowired
    public UsersController(UserService userService,
                           CountryService countryService,
                           ImageService imageService,
                           FriendshipService friendshipService,
                           FriendRequestService friendRequestService,
                           PostTypeService postTypeService,
                           PostService postService,
                           LikeService likeService) {
        this.userService = userService;
        this.countryService = countryService;
        this.imageService = imageService;
        this.friendshipService = friendshipService;
        this.friendRequestService = friendRequestService;
        this.postTypeService = postTypeService;
        this.postService = postService;
        this.likeService = likeService;


    }

    @GetMapping("/{id}")
    public String showProfile(@PathVariable int id, Principal principal, Model model) {

        try {

            if (principal != null) {
                User principalUser = userService.getByUsername(principal.getName());
                model.addAttribute("principalUser", principalUser);
                model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
                model.addAttribute("principalFriends", friendshipService.getFriendsByUserId(principalUser.getId()));
                model.addAttribute("principalToUserRequest", friendRequestService.findRequest(principalUser.getId(), id));
            }

            model.addAttribute("user", userService.getById(id));
            model.addAttribute("friends", friendshipService.getFriendsByUserId(id));
            model.addAttribute("userPosts", postService.getByUser(id));
            model.addAttribute("postToCreate", new Post());
            model.addAttribute("postTypes", postTypeService.getAll());
            model.addAttribute("likeChecker", new LikeChecker(likeService));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("User with id %d not found", id));
        }

        return "profile";
    }

    @GetMapping("/{id}/update")
    public String showUpdateForm(@PathVariable int id, Model model, Principal principal) {
        if (principal != null) {
            User principalUser = userService.getByUsername(principal.getName());
            model.addAttribute("principalUser", principalUser);
            model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
        }
        model.addAttribute("user", userService.getById(id));
        model.addAttribute("userPasswordChangeDTO", new UserPasswordChangeDTO());
        model.addAttribute("countries", countryService.getAll());
        return "profile-edit";
    }

    @PostMapping("/{id}/update")
    public String updateProfile(@PathVariable int id,
                                @RequestParam("imagefile") MultipartFile file,
                                @RequestParam("firstName") String firstName,
                                @RequestParam("lastName") String lastName,
                                @RequestParam("email") String email,
                                @RequestParam("country_id") int countryId,
                                @RequestParam("mobile") String mobile,
                                @RequestParam("birthYear") String birthYear,
                                @RequestParam("contactPrivacy") int contactPrivacy) throws IOException {

        User userToUpdate = userService.getById(id);
        userToUpdate.setFirstName(firstName);
        userToUpdate.setLastName(lastName);
        userToUpdate.getContactInfo().setEmail(email);
        userToUpdate.getContactInfo().setMobile(mobile);
        userToUpdate.getContactInfo().setPrivacy(contactPrivacy);
        userToUpdate.setBirthYear(birthYear);
        userToUpdate.setCountry(countryService.getById(countryId));

        if (!file.isEmpty()) {
            imageService.saveUserImage(id, file);
        }

        userService.update(userToUpdate);
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/changePassword")
    public String changePassword(@PathVariable int id,
                                 @ModelAttribute("userPasswordChangeDTO") UserPasswordChangeDTO userPasswordChangeDTO,
                                 @RequestParam("currentPassword") String currentPassword,
                                 @RequestParam("newPassword") String newPassword,
                                 @RequestParam("newPasswordConfirmation") String newPasswordConfirmation,
                                 RedirectAttributes attributes) {
        userPasswordChangeDTO.setId(id);
        userPasswordChangeDTO.setOldPassword(currentPassword);
        userPasswordChangeDTO.setNewPassword(newPassword);
        userPasswordChangeDTO.setNewPasswordConfirmation(newPasswordConfirmation);
        attributes.addAttribute("passStatus", "successful");

        try {
            userService.changePassword(userPasswordChangeDTO);
        } catch (PasswordMismatchException e) {
            attributes.addAttribute("passStatus", "passwordMismatch");
        } catch (WrongPasswordException e) {
            attributes.addAttribute("passStatus", "wrongPassword");
        }
        return "redirect:/users/{id}/update";

    }

    @GetMapping("/profile")
    public String showMyProfile(Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        }
        User user = userService.getByUsername(principal.getName());
        return String.format("redirect:/users/%d", user.getId());
    }

    @PostMapping("/{id}/update/image")
    public String uploadImage(@PathVariable int id, @RequestParam("imagefile") MultipartFile file) throws IOException {

        imageService.saveUserImage(id, file);

        return "redirect:/users/{id}/update";
    }

    @PostMapping("/{id}/update/background")
    public String uploadBackgroundImage(@PathVariable int id, @RequestParam("imagefile") MultipartFile file) throws IOException {

        imageService.saveUserBackgroundImage(id, file);

        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/update/delete")
    public String deleteUser(@PathVariable int id){

        userService.softDelete(userService.getById(id));

        return "redirect:/admin/users-details";
    }

    @GetMapping("/{id}/profilepicture")
    public void renderProfilePicture(@PathVariable int id, HttpServletResponse response) throws IOException {
        User user = userService.getById(id);

        if (user.getProfilePicture() != null) {
            byte[] byteArray = new byte[user.getProfilePicture().length];
            int i = 0;

            for (Byte wrappedByte : user.getProfilePicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

    @GetMapping("/{id}/background")
    public void renderBackgroundImage(@PathVariable int id, HttpServletResponse response) throws IOException {
        User user = userService.getById(id);

        if (user.getBackgroundPicture() != null) {
            byte[] byteArray = new byte[user.getBackgroundPicture().length];
            int i = 0;

            for (Byte wrappedByte : user.getBackgroundPicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

    @PostMapping("/{id}/accept-friend-request")
    public String acceptFriendRequest(@PathVariable int id, @RequestParam("requestId") int reqeustId) {
        friendRequestService.accept(reqeustId);
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/reject-friend-request")
    public String rejectFriendRequest(@PathVariable int id, @RequestParam("requestId") int reqeustId) {
        friendRequestService.reject(reqeustId);
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/unfriend")
    public String unFriend(@PathVariable int id,
                           @RequestParam("friendId") int friendId,
                           @RequestParam("principalId") int principalId) {
        friendshipService.unFriend(principalId, friendId);
        return "redirect:/users/{id}";
    }

    @PostMapping("/{id}/send-friend-request")
    public String sendFriendRequest(@PathVariable int id,
                                    @RequestParam("userId") int userId,
                                    @RequestParam("principalId") int principalId) {
        friendRequestService.send(principalId, userId);
        return "redirect:/users/{id}";
    }

    @GetMapping("/search")
    public ModelAndView searchUser(@RequestParam(name = "search") String search, Model model){
        model.addAttribute("searchResults", userService.searchUser(search));

        return new ModelAndView("fragments::search-results");
    }
}
