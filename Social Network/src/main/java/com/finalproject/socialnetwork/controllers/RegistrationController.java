package com.finalproject.socialnetwork.controllers;

import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import javax.validation.Valid;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private UserService userService;

    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, UserService userService) {

        this.userDetailsManager = userDetailsManager;
        this.userService = userService;

    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("user", new User());
        return "sign-up";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {

        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            return "sign-up";
        }
        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", "User with that username already exists!");
            return "sign-up";
        }
        if (!user.getPassword().equals(user.getPasswordConfirmation())) {
            model.addAttribute("error", "Password doesn't mach!");
            return "sign-up";
        }
        userService.create(user);

        return "sign-in";
    }

}
