package com.finalproject.socialnetwork.controllers;

import com.finalproject.socialnetwork.exceptions.NotAuthorizedException;
import com.finalproject.socialnetwork.models.AuthorizationValidator;
import com.finalproject.socialnetwork.models.FriendRequest;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.*;
import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;

import static com.finalproject.socialnetwork.Constants.TIME_DIVISION_FACTOR;


@Controller
@RequestMapping("/posts")
public class PostsController {

    private PostService postService;
    private UserService userService;
    private PostTypeService postTypeService;
    private ImageService imageService;
    private FriendRequestService friendRequestService;

    @Autowired
    public PostsController(PostService postService,
                           UserService userService,
                           PostTypeService postTypeService,
                           ImageService imageService,
                           FriendRequestService friendRequestService) {
        this.postService = postService;
        this.userService= userService;
        this.postTypeService = postTypeService;
        this.imageService = imageService;
        this.friendRequestService = friendRequestService;
    }



    @GetMapping("/{id}/create-post")
    public String showPostCreateForm(@PathVariable int id, Model model) {

        model.addAttribute("post", new Post());
        return "redirect:/users/{id}";

    }

    @PostMapping("/{id}/create-post")
    public String  createPost(@RequestParam("description") String description,
                              @RequestParam("isPublic") int isPublic,
                              @RequestParam("userId") int userId,
                              @RequestParam("postType") int postType,
                              @RequestParam("imagefile") MultipartFile file) throws IOException {

        Post post = new Post();
        post.setEnabled(1);
        post.setDescription(description);
        post.setIsPublic(isPublic);
        post.setPostType(postTypeService.getById(postType));
        post.setUser(userService.getById(userId));

        if(!file.isEmpty()){
            post.setPicture(imageService.serializeImage(file));
        }

        postService.create(post);
        post.setPopularity(post.getCreatedAt().getTime()/TIME_DIVISION_FACTOR);
        postService.update(post);
        return "redirect:/users/{id}";
    }

    @GetMapping("/{id}/picture")
    public void renderPostPicture(@PathVariable int id, HttpServletResponse response) throws IOException {
        Post post = postService.getById(id);

        if (post.getPicture() != null) {
            byte[] byteArray = new byte[post.getPicture().length];
            int i = 0;

            for (Byte wrappedByte : post.getPicture()) {
                byteArray[i++] = wrappedByte;
            }

            response.setContentType("image/jpeg");
            InputStream stream = new ByteArrayInputStream(byteArray);
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

    @GetMapping("/{id}/update")
    public String showPostUpdateForm(@PathVariable(name = "id") int postId,
                                     Model model,
                                     Principal principal){

        Post post = postService.getById(postId);
        model.addAttribute("userPost", post);

        if(principal == null){
            throw new ResponseStatusException(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED, "You are not logged in.");
        }else if(userService.getByUsername(principal.getName()).getId() != post.getUser().getId() && !AuthorizationValidator.isAdmin()){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,"You are not the creator of this post nor an admin");
        }

        User principalUser = userService.getByUsername(principal.getName());
        model.addAttribute("principalUser", principalUser);
        model.addAttribute("friendRequests", friendRequestService.getByUserId(principalUser.getId()));
        model.addAttribute("postTypes", postTypeService.getAll());

        return "post-update";
    }


    @PostMapping("/{id}/update")
    public String updatePost(@PathVariable(name = "id") int postId,
                             @RequestParam("description") String description,
                             @RequestParam("imagefile")MultipartFile file,
                             @RequestParam("isPublic") int isPublic,
                             @RequestParam("postType") int postType,
                             Principal principal) throws IOException {

        Post post = postService.getById(postId);

        if(principal == null){
            throw new ResponseStatusException(HttpStatus.NETWORK_AUTHENTICATION_REQUIRED, "You are not logged in.");
        }else if(userService.getByUsername(principal.getName()).getId() != post.getUser().getId() && !AuthorizationValidator.isAdmin()){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,"You are not the creator of this post nor an admin");
        }

        post.setDescription(description);
        post.setIsPublic(isPublic);
        post.setPostType(postTypeService.getById(postType));

        if(!file.isEmpty()){
            post.setPicture(imageService.serializeImage(file));
        }

        postService.update(post);

        return String.format("redirect:/users/%d", post.getUser().getId());
    }
}
