package com.finalproject.socialnetwork.controllers;


import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.services.CommentService;
import com.finalproject.socialnetwork.services.PostService;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import static com.finalproject.socialnetwork.Constants.MAX_COMMENTS_PER_POST;

@Controller
@RequestMapping("/comments")
public class CommentsController {

    private CommentService commentService;
    private UserService userService;
    private PostService postService;

    @Autowired
    public CommentsController(CommentService commentService, UserService userService, PostService postService) {
        this.commentService = commentService;
        this.userService = userService;
        this.postService = postService;
    }

    @RequestMapping(value = "/post-comment", method = RequestMethod.POST)
    public ModelAndView postComment(@RequestParam(name = "postId") int postId,
                                    @RequestParam(name = "comment") String commentMessage,
                                    @RequestParam(name = "principalId") int principalId,
                                    Model model) {


        Comment comment = new Comment();
        comment.setMessage(commentMessage);
        comment.setUser(userService.getById(principalId));

        Post post = postService.getById(postId);

        comment.setPost(post);
        commentService.create(comment);
        postService.updatePopularity(postId);
        model.addAttribute("comment", comment);

        return new ModelAndView("fragments::new-comment");
    }
}
