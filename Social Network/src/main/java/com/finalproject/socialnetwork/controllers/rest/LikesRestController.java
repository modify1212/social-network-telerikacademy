package com.finalproject.socialnetwork.controllers.rest;
import com.finalproject.socialnetwork.exceptions.DuplicateEntityException;
import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.services.LikeService;
import com.finalproject.socialnetwork.services.PostService;
import com.finalproject.socialnetwork.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;


@RestController
@RequestMapping("api/likes")
public class LikesRestController {

    private LikeService likeService;
    private UserService userService;
    private PostService postService;

    public LikesRestController(LikeService likeService, UserService userService, PostService postService ) {
        this.likeService = likeService;
        this.userService = userService;
        this.postService = postService;
    }


    @PostMapping("/like-post")
    public int like(@RequestParam("userId") int userId,
                    @RequestParam("postId") int postId){

        Like like = new Like();
        try {
            like.setUser(userService.getById(userId));
            like.setPost(postService.getById(postId));
        } catch (EntityNotFoundException e) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        try {
            likeService.create(like);
        } catch (DuplicateEntityException e) {
            return unlike(userId, postId);
        }
        postService.updatePopularity(postId);
        return like.getPost().getLikes().size();
    }


    private int unlike (int userId, int postId){

        try {
            Like like = likeService.getByUserIdAndPostId(userId,postId);
            likeService.delete(like);
        } catch (EntityNotFoundException e) {
           throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }

        return postService.getById(postId).getLikes().size();
    }
}
