package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LikeRepository extends JpaRepository<Like, Integer> {

    List<Like> findAllByPostId( int postId);

    boolean existsByUserIdAndPostId (int userId, int postId);

    @Query("select l from Like l where l.user.id =:userId and l.post.id=:postId")
    Like getByUserIdAndPostId(int userId, int postId);
}
