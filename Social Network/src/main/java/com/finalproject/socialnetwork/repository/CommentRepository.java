package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

    List<Comment> findAllByUserId(int userId);

    List<Comment> findAllByPostId(int postId);
}
