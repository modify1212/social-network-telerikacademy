package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.PostType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostTypeRepository extends JpaRepository<PostType, Integer> {
}
