package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Integer>{

    List<Post> findAllByUserIdOrderByIdDesc(int userId);

    Post getById(int id);

    @Query("select p from Post p order by p.popularity desc")
    Page<Post> getAll(Pageable pageable);

    Page<Post> findAllByPostTypeIdOrderByPopularityDesc(Pageable pageable, int postTypeId);
}
