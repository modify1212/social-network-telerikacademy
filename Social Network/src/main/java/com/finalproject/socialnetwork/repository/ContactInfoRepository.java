package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.ContactInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactInfoRepository extends JpaRepository<ContactInfo, Integer> {
}
