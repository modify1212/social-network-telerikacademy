package com.finalproject.socialnetwork.repository;


import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.models.TeamRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeamRequestRepository extends JpaRepository<TeamRequest, Integer> {

    List<TeamRequest> findByTeamId(int teamId);

    boolean existsByTeamIdAndUserId(int teamId, int userId);
}
