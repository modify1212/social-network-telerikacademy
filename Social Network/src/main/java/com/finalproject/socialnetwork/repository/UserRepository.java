package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

    User getById(int id);

    User getByUsername(String username);

    List<User> findAllByTeamId(int teamId);

    @Query("select u from User u where u.firstName like %:search% or u.lastName like %:search%")
    List<User> searchUser(@Param("search") String search);
}
