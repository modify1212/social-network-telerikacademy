package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer>{

    Team getById(int id);
}
