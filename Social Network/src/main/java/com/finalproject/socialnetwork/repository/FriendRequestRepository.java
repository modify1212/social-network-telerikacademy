package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.FriendRequest;
import com.finalproject.socialnetwork.models.Friendship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendRequestRepository  extends JpaRepository<FriendRequest, Integer> {

    List<FriendRequest> findByRecipientIdOrderByIdDesc(int recipientId);

    FriendRequest findBySenderIdAndRecipientId(int senderId, int recipientId);

    @Query("select fr from FriendRequest fr where fr.sender.id = :senderId and fr.recipient.id = :recipientId or fr.sender.id = :recipientId and fr.recipient.id= :senderId")
    List<FriendRequest> findRequest(int senderId, int recipientId);
}
