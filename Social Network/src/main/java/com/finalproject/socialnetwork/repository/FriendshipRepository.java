package com.finalproject.socialnetwork.repository;

import com.finalproject.socialnetwork.models.Friendship;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Integer> {

    List<Friendship> findAllByFriendOneId(int id);
    List<Friendship> findAllByFriendTwoId(int id);

    @Query ("select f from Friendship f  where f.friendOne.id = :principalId and f.friendTwo.id = :friendId or f.friendOne.id = :friendId and f.friendTwo.id= :principalId")
    List<Friendship> findFriendship(int principalId, int friendId);
}
