package com.finalproject.socialnetwork.repository;


import com.finalproject.socialnetwork.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

    Country getById(int id);

}
