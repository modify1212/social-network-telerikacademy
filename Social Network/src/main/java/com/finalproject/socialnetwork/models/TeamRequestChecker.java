package com.finalproject.socialnetwork.models;

import com.finalproject.socialnetwork.services.TeamRequestService;

public class TeamRequestChecker {

    private TeamRequestService teamRequestService;

    public TeamRequestChecker(TeamRequestService teamRequestService) {
        this.teamRequestService = teamRequestService;
    }

    public boolean requestExists(int teamId, int userId){
        return teamRequestService.checkRequestExists(teamId,userId);
    }
}
