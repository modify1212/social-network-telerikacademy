package com.finalproject.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "friends")
public class Friendship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "friendship_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "friend_one")
    private User friendOne;

    @OneToOne
    @JoinColumn(name = "friend_two")
    private User friendTwo;


    public Friendship() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public User getFriendOne() {
        return friendOne;
    }

    public User getFriendTwo() {
        return friendTwo;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setFriendOne(User friendOne) {
        this.friendOne = friendOne;
    }

    public void setFriendTwo(User friendTwo) {
        this.friendTwo = friendTwo;
    }
}
