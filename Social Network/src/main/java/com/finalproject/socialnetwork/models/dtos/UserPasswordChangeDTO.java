package com.finalproject.socialnetwork.models.dtos;

public class UserPasswordChangeDTO {

    private int id;
    private String oldPassword;
    private String newPassword;
    private String newPasswordConfirmation;


    public UserPasswordChangeDTO() {
    }


    //Getters
    public int getId() {
        return id;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getNewPasswordConfirmation() {
        return newPasswordConfirmation;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setNewPasswordConfirmation(String newPasswordConfirmation) {
        this.newPasswordConfirmation = newPasswordConfirmation;
    }
}
