package com.finalproject.socialnetwork.models;

import com.finalproject.socialnetwork.services.LikeService;
import javax.persistence.EntityNotFoundException;


public class LikeChecker {

    private LikeService likeService;


    public LikeChecker(LikeService likeService) {
        this.likeService = likeService;
    }

    public boolean likeExists(int userId, int postId){

        try {
            Like like = likeService.getByUserIdAndPostId(userId, postId);
            if(like != null){
                return true;
            }
        } catch (EntityNotFoundException e) {
            return false;
        }

        return false;
    }
}
