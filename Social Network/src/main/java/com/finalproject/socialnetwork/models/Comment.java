package com.finalproject.socialnetwork.models;


import javax.persistence.*;

@Entity
@Table(name = "comments")
public class Comment extends AuditModel{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    private int id;

    @Column(name = "comment")
    private String message;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Transient
    private String timeAgo;

    public Comment() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public Post getPost() {
        return post;
    }

    public User getUser() {
        return user;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }
}
