package com.finalproject.socialnetwork.models;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.CollectionId;
import org.hibernate.annotations.Where;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "posts")
@Where(clause = "enabled = 1")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Post extends AuditModel{


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "post_id")
    private int id;

    @Column(name = "description")
    private String description;

    @JsonIgnore
    @Column(name = "picture")
    private Byte[] picture;

    @Column(name = "is_public")
    private int isPublic;

    @Column(name = "enabled")
    private int enabled;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "post_type")
    private PostType postType;

    @Column(name = "popularity")
    private long popularity;

    @OneToMany(mappedBy = "post",
            fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Comment> comments = new ArrayList<>();

    @Transient
    @JsonIgnore
    private List<Like> likes = new ArrayList<>();

    @Transient
    private String timeAgo;

    public Post() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public int getIsPublic() {
        return isPublic;
    }

    public int getEnabled() {
        return enabled;
    }

    public User getUser() {
        return user;
    }

    public PostType getPostType() {
        return postType;
    }

    public long getPopularity() {
        return popularity;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public List<Like> getLikes() {
        return likes;
    }

    public String getTimeAgo() {
        return timeAgo;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public void setIsPublic(int isPublic) {
        this.isPublic = isPublic;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setPostType(PostType postType) {
        this.postType = postType;
    }

    public void setPopularity(long popularity) {
        this.popularity = popularity;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setLikes(List<Like> likes) {
        this.likes = likes;
    }

    public void setTimeAgo(String timeAgo) {
        this.timeAgo = timeAgo;
    }
}

