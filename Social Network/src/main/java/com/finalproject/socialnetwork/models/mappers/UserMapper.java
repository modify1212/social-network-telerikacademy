package com.finalproject.socialnetwork.models.mappers;

import com.finalproject.socialnetwork.models.ContactInfo;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.UserRepository;
import com.finalproject.socialnetwork.services.ContactInfoService;
import com.finalproject.socialnetwork.services.CountryService;
import com.finalproject.socialnetwork.services.TeamService;
import com.finalproject.socialnetwork.services.UserService;
import com.finalproject.socialnetwork.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Component;


import java.util.List;

import static com.finalproject.socialnetwork.Constants.*;
import static com.finalproject.socialnetwork.Constants.DEFAULT_TEAM_ID;

@Component
public class UserMapper {


    private ContactInfoService contactInfoService;
    private CountryService countryService;
    private TeamService teamService;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserDetailsManager userDetailsManager;



     public UserMapper(ContactInfoService contactInfoService,
                      CountryService countryService,
                      TeamService teamService,
                      UserRepository userRepository,
                      PasswordEncoder passwordEncoder,
                      UserDetailsManager userDetailsManager
                     ) {
        this.contactInfoService = contactInfoService;
        this.countryService = countryService;
        this.teamService = teamService;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsManager = userDetailsManager;

    }
    @Autowired
    public UserMapper() {
    }

    public User registeredUserToUser(User user) {
        User userToCreate = new User();
        ContactInfo contactInfo = new ContactInfo();

        userToCreate.setUsername(user.getUsername());
        userToCreate.setFirstName(user.getFirstName());
        userToCreate.setLastName(user.getLastName());
        userToCreate.setPassword(user.getPassword());

        contactInfo.setEmail(userToCreate.getUsername());
        contactInfo.setMobile(DEFAULT_MOBILE);
        contactInfo.setPrivacy(1);

        contactInfoService.create(contactInfo);
        userToCreate.setContactInfo(contactInfo);
        userToCreate.setBirthYear(DEFAULT_BIRTH_YEAR);
        userToCreate.setCountry(countryService.getById(DEFAULT_COUNTRY_ID));
        userToCreate.setTeam(teamService.getById(DEFAULT_TEAM_ID));
        User savedUser = userRepository.saveAndFlush(userToCreate);
        createSpringSecurityUser(user);
        UserUtils.sendEmailToUser(user.getUsername(), user.getLastName());

        return savedUser;

    }

    private void createSpringSecurityUser(User user) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        passwordEncoder.encode(user.getPassword()),
                        authorities);

        userDetailsManager.updateUser(newUser);
    }
}
