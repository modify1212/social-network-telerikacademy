package com.finalproject.socialnetwork.models;


import javax.persistence.*;

@Entity
@Table(name = "contact_infos")
public class ContactInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contact_info_id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "mobile")
    private String mobile;

    @Column(name = "privacy")
    private int privacy;


    public ContactInfo() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getMobile() {
        return mobile;
    }

    public int getPrivacy() {
        return privacy;
    }

    //Setters

    public void setId(int id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setPrivacy(int privacy) {
        this.privacy = privacy;
    }

}
