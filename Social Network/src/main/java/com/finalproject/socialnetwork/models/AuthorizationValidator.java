package com.finalproject.socialnetwork.models;


import org.springframework.security.core.context.SecurityContextHolder;

public class AuthorizationValidator {

    public static boolean isAdmin() {
        return SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals("ROLE_ADMIN"));
    }
}
