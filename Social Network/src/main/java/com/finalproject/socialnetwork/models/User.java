package com.finalproject.socialnetwork.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "users")
@Where(clause = "enabled = 1")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private int id;

    @Column(name = "username")
    private String username;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private int enabled;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToOne
    @JoinColumn (name = "contact_info_id")
    private ContactInfo contactInfo;

    @OneToOne
    @JoinColumn(name = "country")
    private Country country;

    @Column(name = "birth_year")
    private String birthYear;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;

    @JsonIgnore
    @Column(name = "picture")
    private Byte[] profilePicture;

    @JsonIgnore
    @Column(name = "background_picture")
    private Byte[] backgroundPicture;

    @JsonIgnore
    @Transient
    private String passwordConfirmation;

    @JsonIgnore
    @Transient
    private List<User> friends;

    public User() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getEnabled() {
        return enabled;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public Country getCountry() {
        return country;
    }

    public String getBirthYear() {
        return birthYear;
    }

    public Team getTeam() {
        return team;
    }

    public Byte[] getProfilePicture() {
        return profilePicture;
    }

    public Byte[] getBackgroundPicture() {
        return backgroundPicture;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public List<User> getFriends() {
        return friends;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    public void setCountry(Country country){this.country=country;}

    public void setBirthYear(String birthYear) {
        this.birthYear = birthYear;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public void setProfilePicture(Byte[] profilePicture) {
        this.profilePicture = profilePicture;
    }

    public void setBackgroundPicture(Byte[] backgroundPicture) {
        this.backgroundPicture = backgroundPicture;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }
}
