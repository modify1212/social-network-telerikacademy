package com.finalproject.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "post_types")
public class PostType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "post_type_id")
    private int id;

    @Column(name = "post_type")
    private String postType;

    public PostType() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getPostType() {
        return postType;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }
}
