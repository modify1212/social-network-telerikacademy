package com.finalproject.socialnetwork.models;

import javax.persistence.*;

@Entity
@Table(name = "friend_requests" )
public class FriendRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "friend_request_id")
    private int id;

    @OneToOne
    @JoinColumn(name = "sender")
    private User sender;

    @OneToOne
    @JoinColumn(name = "recipient")
    private User recipient;

    public FriendRequest() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public User getSender() {
        return sender;
    }

    public User getRecipient() {
        return recipient;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setSender(User senderId) {
        this.sender = senderId;
    }

    public void setRecipient(User recipientId) {
        this.recipient = recipientId;
    }
}
