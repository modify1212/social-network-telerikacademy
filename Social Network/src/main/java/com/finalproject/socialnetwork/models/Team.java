package com.finalproject.socialnetwork.models;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "teams")
@Where(clause = "enabled = 1")
public class Team {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "team_id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "enabled")
    private int enabled;

    @Column(name = "description")
    private String description;

    @Column (name = "attitude")
    private int attitude;

    @JsonIgnore
    @Column(name = "picture")
    private Byte[] picture;

    @JsonIgnore
    @Column(name = "background_picture")
    private Byte[] backgroundPicture;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User creator;

    @OneToMany(mappedBy = "team",
            fetch = FetchType.LAZY)
    @JsonIgnore
    private List<User> members;

    public Team() {
    }

    //Getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getEnabled() {
        return enabled;
    }

    public String getDescription() {
        return description;
    }

    public int getAttitude() {
        return attitude;
    }

    public Byte[] getPicture() {
        return picture;
    }

    public Byte[] getBackgroundPicture() {
        return backgroundPicture;
    }

    public User getCreator() {
        return creator;
    }

    public List<User> getMembers() {
        return members;
    }

    //Setters
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAttitude(int attitude) {
        this.attitude = attitude;
    }

    public void setPicture(Byte[] picture) {
        this.picture = picture;
    }

    public void setBackgroundPicture(Byte[] backgroundPicture) {
        this.backgroundPicture = backgroundPicture;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }
}
