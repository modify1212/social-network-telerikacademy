package com.finalproject.socialnetwork.exceptions;


public class DuplicateEntityException extends RuntimeException {

    public DuplicateEntityException(String message){
        super(message);
    }
}
