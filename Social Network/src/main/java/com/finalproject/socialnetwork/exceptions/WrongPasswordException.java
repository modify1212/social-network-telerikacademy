package com.finalproject.socialnetwork.exceptions;

public class WrongPasswordException extends RuntimeException{

    private static final String WRONG_PASSWORD_MESSAGE = "Wrong password";

    public WrongPasswordException() {
        super(WRONG_PASSWORD_MESSAGE);

    }
}
