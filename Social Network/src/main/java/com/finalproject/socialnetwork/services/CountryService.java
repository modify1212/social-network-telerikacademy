package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAll();

    Country getById(int id);
}
