package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Team;

import java.util.List;

public interface TeamService {

    Team create (Team team);

    Team update (Team team);

    void softDelete(Team team);

    void delete(Team team);

    Team getById(int id);

    List<Team> getAll();




}
