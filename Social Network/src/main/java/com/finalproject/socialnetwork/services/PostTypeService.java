package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.PostType;

import java.util.List;

public interface PostTypeService {

    List<PostType> getAll();

    PostType getById(int id);
}
