package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.models.User;
import com.sun.org.apache.xpath.internal.operations.Mult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import javax.transaction.Transactional;
import java.io.IOException;

@Service
public class ImageServiceImpl implements ImageService{

    private UserService userService;
    private TeamService teamService;


    @Autowired
    public ImageServiceImpl( UserService userService, TeamService teamService) {
        this.userService = userService;
        this.teamService = teamService;
    }

    @Transactional
    public void saveUserImage(int id, MultipartFile image) throws IOException {

        User user = userService.getById(id);

        Byte[] byteObject = serializeImage(image);
        user.setProfilePicture(byteObject);
        userService.update(user);
    }

    @Transactional
    public void saveTeamImage(Team team, MultipartFile image) throws IOException {

        Byte[] byteObject = serializeImage(image);
        team.setPicture(byteObject);
        teamService.update(team);
    }

    @Transactional
    public void saveUserBackgroundImage(int id, MultipartFile image) throws IOException {

        User user = userService.getById(id);

        Byte[] byteObject = serializeImage(image);
        user.setBackgroundPicture(byteObject);
        userService.update(user);
    }

    @Transactional
    public void saveTeamBackgroundImage(int teamId, MultipartFile image) throws IOException{
        Team team = teamService.getById(teamId);

        Byte[] byteObject = serializeImage(image);
        team.setBackgroundPicture(byteObject);
        teamService.update(team);
    }

    @Override
    public Byte[] serializeImage(MultipartFile image) throws IOException {

        Byte[] byteObject = new Byte[image.getBytes().length];
        int i = 0;
        for (byte b : image.getBytes()){
            byteObject[i++] = b;
        }
        return byteObject;
    }
}