package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.User;

import java.util.List;

public interface FriendshipService {

    List<User> getFriendsByUserId(int id);
    void unFriend(int principalId, int friendId);
}
