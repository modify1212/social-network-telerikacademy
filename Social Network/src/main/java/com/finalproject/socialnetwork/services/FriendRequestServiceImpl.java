package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.exceptions.DuplicateEntityException;
import com.finalproject.socialnetwork.models.FriendRequest;
import com.finalproject.socialnetwork.models.Friendship;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.FriendRequestRepository;
import com.finalproject.socialnetwork.repository.FriendshipRepository;
import com.finalproject.socialnetwork.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class FriendRequestServiceImpl implements FriendRequestService {

    private FriendRequestRepository friendRequestRepository;
    private FriendshipRepository friendshipRepository;
    private UserRepository userRepository;

    @Autowired
    public FriendRequestServiceImpl(FriendRequestRepository friendRequestRepository,
                                    FriendshipRepository friendshipRepository,
                                    UserRepository userRepository) {
        this.friendRequestRepository = friendRequestRepository;
        this.friendshipRepository = friendshipRepository;
        this.userRepository = userRepository;
    }
    @Override
    public void delete (FriendRequest friendRequest) {
        friendRequestRepository.delete(friendRequest);
    }

    @Override
    public List<FriendRequest> getByUserId(int id) {
        return friendRequestRepository
                .findByRecipientIdOrderByIdDesc(id);
    }

    @Override
    public List<FriendRequest> findRequest(int senderId, int recipientId){
        return friendRequestRepository
                .findRequest(senderId, recipientId);
    }

    @Override
    @Transactional
    public void accept(int id) {
        Friendship friendship = new Friendship();
        FriendRequest friendRequest = friendRequestRepository.getOne(id);

        friendship.setFriendOne(friendRequest.getSender());
        friendship.setFriendTwo(friendRequest.getRecipient());

        friendshipRepository.saveAndFlush(friendship);
        friendRequestRepository.delete(friendRequest);
    }

    @Override
    public void reject(int id) {
        friendRequestRepository.delete(friendRequestRepository.getOne(id));
    }

    @Override
    public void send(int senderId, int recipientId) {
        String exceptionMessage = "Request already sent";
        if(friendRequestRepository.findBySenderIdAndRecipientId(senderId, recipientId) != null ){
            throw new DuplicateEntityException(exceptionMessage);
        } else if (friendRequestRepository.findBySenderIdAndRecipientId(recipientId, senderId) != null ){
            throw new DuplicateEntityException(exceptionMessage);
        } else{

            FriendRequest friendRequest = new FriendRequest();
            User sender = userRepository.getById(senderId);
            User recipient = userRepository.getById(recipientId);

            friendRequest.setSender(sender);
            friendRequest.setRecipient(recipient);

            friendRequestRepository.saveAndFlush(friendRequest);
        }
    }
}
