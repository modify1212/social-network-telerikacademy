package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepository commentRepository;


    @Autowired
    public CommentServiceImpl(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Override
    public Comment create(Comment comment) {

        commentRepository.saveAndFlush(comment);
        return comment;
    }

    @Override
    public Comment update(Comment comment) {
        commentRepository.saveAndFlush(comment);
        return comment;

    }

    @Override
    public void delete(Comment comment) {

        commentRepository.delete(comment);
    }

    @Override
    public Comment getById(int id) {
        return commentRepository.getOne(id);
    }

    @Override
    public List<Comment> getAll() {
        return commentRepository.findAll();
    }

    @Override
    public List<Comment> getByPost(int postId) {
        return commentRepository.findAllByPostId(postId);
    }

    @Override
    public List<Comment> getByUser(int userId) {
        return commentRepository.findAllByUserId(userId);
    }
}
