package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.exceptions.PasswordMismatchException;
import com.finalproject.socialnetwork.exceptions.WrongPasswordException;
import com.finalproject.socialnetwork.models.ContactInfo;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.models.dtos.UserPasswordChangeDTO;
import com.finalproject.socialnetwork.models.mappers.UserMapper;
import com.finalproject.socialnetwork.repository.ContactInfoRepository;
import com.finalproject.socialnetwork.repository.UserRepository;
import com.finalproject.socialnetwork.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.OneToOne;
import java.util.List;

import static com.finalproject.socialnetwork.Constants.*;

@Service
public class UserServiceImpl implements UserService {
    private UserDetailsManager userDetailsManager;
    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private CountryService countryService;
    private TeamService teamService;
    private ContactInfoService contactInfoService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           UserDetailsManager userDetailsManager,
                           CountryService countryService,
                           TeamService teamService,
                           ContactInfoService contactInfoService) {
        this.userRepository = userRepository;
        this.passwordEncoder=passwordEncoder;
        this.userDetailsManager=userDetailsManager;
        this.countryService = countryService;
        this.teamService = teamService;
        this.contactInfoService = contactInfoService;

    }


    @Override
    public User create(User user){
        UserMapper userMapper = new UserMapper(
                contactInfoService,
                countryService,
                teamService,
                userRepository,
                passwordEncoder,
                userDetailsManager
        );
        return   userMapper.registeredUserToUser(user);
    }

    @Override
    public User update(User user){
        userRepository.saveAndFlush(user);
        return user;
    }

    @Override
    public void softDelete(User user){

        user.setEnabled(0);
        userRepository.saveAndFlush(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    @Override
    public User getById(int id){
        User user = userRepository.getById(id);
        if(user==null){ throw new EntityNotFoundException(String.format("User with ID %d, not found", id));
        }

        return user;
    }

    @Override
    public User getByUsername(String username){
        User user = userRepository.getByUsername(username);
        if(user==null){
            throw new EntityNotFoundException();
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> getByTeam(int teamId){return userRepository.findAllByTeamId(teamId);}

    @Override
    public List<User> searchUser(String search) {
        return userRepository.searchUser(search);
    }

    @Override
    public void  changePassword(UserPasswordChangeDTO userPasswordChangeDTO){

        User originalUser= getById(userPasswordChangeDTO.getId());

        if (!passwordEncoder.matches(userPasswordChangeDTO.getOldPassword(),originalUser.getPassword())){
            throw new WrongPasswordException();
        }
        if (!userPasswordChangeDTO.getNewPassword().equals(userPasswordChangeDTO.getNewPasswordConfirmation())){
            throw new PasswordMismatchException();
        }
       String newHashedPassword=passwordEncoder.encode(userPasswordChangeDTO.getNewPassword());
        originalUser.setPassword(newHashedPassword);
        userRepository.saveAndFlush(originalUser);

    }

    @Override
    public void leaveTeam(int userId){
        User user = getById(userId);
        user.setTeam(teamService.getById(1));
        update(user);
    }

}

