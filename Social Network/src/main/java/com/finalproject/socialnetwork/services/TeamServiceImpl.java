package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeamServiceImpl implements TeamService {

    private TeamRepository teamRepository;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }


    @Override
    public Team create(Team team) {
        teamRepository.saveAndFlush(team);
        return team;
    }

    @Override
    public Team update(Team team) {
        teamRepository.saveAndFlush(team);
        return team;
    }

    @Override
    public void softDelete(Team team) {
        team.setEnabled(0);
        teamRepository.saveAndFlush(team);
    }

    @Override
    public void delete(Team team) {
        teamRepository.delete(team);
    }

    @Override
    public Team getById(int id) {
        return teamRepository.getById(id);
    }

    @Override
    public List<Team> getAll() {
        return teamRepository.findAll();
    }

}
