package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.models.dtos.UserPasswordChangeDTO;

import java.util.List;

public interface UserService {

    User create(User user);

    User update(User user);

    void  changePassword(UserPasswordChangeDTO userPasswordChangeDTO);

    void softDelete(User user);

    void delete(User user);

    User getById(int id);

    User getByUsername(String username);

    List<User> getAll();

    List<User> getByTeam(int teamId);

    List<User> searchUser(String search);

    void leaveTeam(int userId);

}
