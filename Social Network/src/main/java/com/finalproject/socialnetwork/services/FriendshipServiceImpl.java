package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Friendship;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.FriendshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FriendshipServiceImpl implements FriendshipService {

    private FriendshipRepository friendshipRepository;


    @Autowired
    public FriendshipServiceImpl(FriendshipRepository friendshipRepository) {
        this.friendshipRepository = friendshipRepository;

    }

    @Override
    public List<User> getFriendsByUserId(int id){

        List<User> friends = new ArrayList<>();
        for (Friendship friendship: friendshipRepository.findAllByFriendOneId(id)) {
            friends.add(friendship.getFriendTwo());
        }
        for (Friendship friendship: friendshipRepository.findAllByFriendTwoId(id)) {
            friends.add(friendship.getFriendOne());
        }
        return friends;
    }
    @Override
    public void unFriend(int principalId, int friendId){

        List<Friendship> friendship = friendshipRepository.findFriendship(principalId,friendId);
        if(friendship.isEmpty()){
            throw new EntityNotFoundException("Friendship not found");
        }

        friendshipRepository.delete(friendship.get(0));
    }
}
