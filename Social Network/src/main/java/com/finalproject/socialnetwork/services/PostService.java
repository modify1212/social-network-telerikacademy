package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import java.util.List;

public interface PostService {

    Post create(Post post);

    Post update(Post post);

    void delete(Post post);

    void softDelete(Post post);

    Post getById(int id);

    Page<Post> getFeed(Pageable pageable);

    Page<Post> getByTypeId(Pageable pageable, int postTypeId);

    List<Post> getAll();

    List<Post> getByUser(int id);

    void updatePopularity(int postId);
}
