package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.FriendRequest;

import java.util.List;

public interface FriendRequestService {

    List<FriendRequest> getByUserId(int id);
    List<FriendRequest> findRequest(int senderId, int recipientId);
    void delete (FriendRequest friendRequest);
    void accept(int id);
    void reject(int id);
    void send(int senderId, int recipientId);
}
