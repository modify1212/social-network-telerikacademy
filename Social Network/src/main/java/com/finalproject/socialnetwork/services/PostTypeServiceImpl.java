package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.PostType;
import com.finalproject.socialnetwork.repository.PostTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostTypeServiceImpl implements PostTypeService {

    private PostTypeRepository postTypeRepository;

    @Autowired
    public PostTypeServiceImpl(PostTypeRepository postTypeRepository) {
        this.postTypeRepository = postTypeRepository;
    }

    @Override
    public List<PostType> getAll(){
        return postTypeRepository.findAll();
    }

    @Override
    public PostType getById(int id){
        return postTypeRepository.getOne(id);
    }
}
