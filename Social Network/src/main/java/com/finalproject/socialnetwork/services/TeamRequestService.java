package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.TeamRequest;

import java.util.List;

public interface TeamRequestService {

    List<TeamRequest> getAll();

    List<TeamRequest> getByTeam(int teamId);

    TeamRequest getById(int requestId);

    void send(int teamId, int userId);

    void reject(int requestId);

    void accept(int requestId);

    void delete(int requestId);

    boolean checkRequestExists(int teamId, int userId);
}
