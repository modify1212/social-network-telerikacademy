package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.TeamRequest;
import com.finalproject.socialnetwork.repository.TeamRepository;
import com.finalproject.socialnetwork.repository.TeamRequestRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class TeamRequestServiceImpl implements TeamRequestService {

    private TeamRequestRepository teamRequestRepository;
    private TeamService teamService;
    private UserService userService;

    public TeamRequestServiceImpl(TeamRequestRepository teamRequestRepository, TeamService teamService, UserService userService) {
        this.teamRequestRepository = teamRequestRepository;
        this.teamService = teamService;
        this.userService = userService;
    }

    @Override
    public List<TeamRequest> getAll(){
        return teamRequestRepository.findAll();
    }

    @Override
    public TeamRequest getById(int requestId){
        return teamRequestRepository.getOne(requestId);
    }

    @Override
    public List<TeamRequest> getByTeam(int teamId){
        return teamRequestRepository.findByTeamId(teamId);
    }

    @Override
    public void reject(int requestId){
        delete(requestId);
    }

    @Override
    @Transactional
    public void accept(int requestId){
        TeamRequest teamRequest = getById(requestId);
        teamRequest.getUser().setTeam(teamService.getById(teamRequest.getTeam().getId()));
        delete(requestId);
    }

    @Override
    public void send(int teamId, int userId){
        TeamRequest teamRequest = new TeamRequest();
        teamRequest.setTeam(teamService.getById(teamId));
        teamRequest.setUser(userService.getById(userId));
        teamRequestRepository.saveAndFlush(teamRequest);
    }

    @Override
    public void delete(int requestId){
        teamRequestRepository.delete(getById(requestId));
    }

    @Override
    public boolean checkRequestExists(int teamId, int userId) {
        return teamRequestRepository.existsByTeamIdAndUserId(teamId, userId);
    }
}
