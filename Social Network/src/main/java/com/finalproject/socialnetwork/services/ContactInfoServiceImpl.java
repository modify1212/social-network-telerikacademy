package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.ContactInfo;
import com.finalproject.socialnetwork.repository.ContactInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ContactInfoServiceImpl implements ContactInfoService {

    private ContactInfoRepository contactInfoRepository;

    @Autowired
    public ContactInfoServiceImpl(ContactInfoRepository contactInfoRepository) {
        this.contactInfoRepository = contactInfoRepository;
    }

    public ContactInfo create(ContactInfo contactInfo){
        return contactInfoRepository.saveAndFlush(contactInfo);
    }
    @Override
    public ContactInfo getById(int id){
       return contactInfoRepository.getOne(id);
    }



}
