package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Comment;

import java.util.List;

public interface CommentService {

    Comment  create(Comment comment);

    Comment update(Comment comment);

    void delete(Comment comment);

    Comment getById(int id);

    List<Comment>getAll();

    List<Comment> getByPost(int postId);

    List<Comment> getByUser(int userId);

}
