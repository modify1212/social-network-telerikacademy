package com.finalproject.socialnetwork.services;



import com.finalproject.socialnetwork.models.Like;

import java.util.List;

public interface LikeService {

    Like create(Like like);

    void delete(Like like);

    Like getById(int id);

    Like getByUserIdAndPostId(int userId, int postId);

    public List<Like> getByPost(int postId);
}
