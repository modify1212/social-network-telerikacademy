package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Team;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface ImageService {

    void saveUserImage(int id, MultipartFile image) throws IOException;

    void saveTeamImage(Team team, MultipartFile image) throws IOException;

    void saveUserBackgroundImage(int id, MultipartFile image) throws IOException;

    void saveTeamBackgroundImage(int teamId, MultipartFile image) throws IOException;

    Byte[] serializeImage(MultipartFile image) throws IOException;
}
