package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.TimeAgo;
import com.finalproject.socialnetwork.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.finalproject.socialnetwork.Constants.COMMENT_MULTIPLICATION_FACTOR;
import static com.finalproject.socialnetwork.Constants.LIKE_MULTIPLICATION_FACTOR;

@Service
public class PostServiceImpl implements PostService {

    private PostRepository postRepository;
    private LikeService likeService;
    private FriendshipService friendshipService;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, LikeService likeService, FriendshipService friendshipService) {
        this.postRepository = postRepository;
        this.likeService = likeService;
        this.friendshipService = friendshipService;
    }

    @Override
    public Post create(Post post) {
        postRepository.saveAndFlush(post);
        return post;
    }

    @Override
    public Post update(Post post) {
        postRepository.saveAndFlush(post);
        return post;
    }

    @Override
    public void softDelete(Post post) {
        post.setEnabled(0);
        postRepository.saveAndFlush(post);
    }

    @Override
    public void delete(Post post) {
        postRepository.delete(post);
    }

    @Override
    public Post getById(int id){
        Post post = postRepository.getById(id);
        if(post == null){
            throw new EntityNotFoundException(String.format("Post with ID %d, not found", id));
        }
        post.setLikes(likeService.getByPost(id));
       return post;
    }

    @Override
    public Page<Post> getFeed(Pageable pageable){

        Page<Post> posts = postRepository.getAll(pageable);
        populatePageInformation(posts);
        return posts;
    }

    @Override
    public Page<Post> getByTypeId(Pageable pageable, int postTypeId){
        Page<Post> posts = postRepository.findAllByPostTypeIdOrderByPopularityDesc(pageable, postTypeId);
        populatePageInformation(posts);
//        if(posts.isEmpty()){
//            throw new EntityNotFoundException("No posts of this type");
//        }
        return posts;
    }

    @Override
    public List<Post> getAll() {

        List<Post> posts = postRepository.findAll();
        populatePostInformation(posts);
        return posts;
    }

    @Override
    public List<Post> getByUser(int id){

        List<Post> posts = postRepository.findAllByUserIdOrderByIdDesc(id);
        populatePostInformation(posts);
        return posts;
    }

    public void updatePopularity(int postId){
        Post post = getById(postId);
        post.setLikes(likeService.getByPost(postId));
        long popularity = post.getPopularity() + post.getLikes().size()*LIKE_MULTIPLICATION_FACTOR + post.getComments().size()*COMMENT_MULTIPLICATION_FACTOR;
        post.setPopularity(popularity);
        update(post);
    }

    private void populatePostInformation(List<Post> posts){

        posts.forEach(p-> {p.getUser().setFriends(friendshipService.getFriendsByUserId(p.getUser().getId()));
                            p.setLikes(likeService.getByPost(p.getId()));
                            p.getComments().forEach(c -> c.setTimeAgo(TimeAgo.calculateCommentTimeAgo(c)));
                            p.setTimeAgo(TimeAgo.calculatePostTimeAgo(p));});

    }

    private void populatePageInformation(Page<Post> posts){
        posts.forEach(p-> {p.getUser().setFriends(friendshipService.getFriendsByUserId(p.getUser().getId()));
            p.setLikes(likeService.getByPost(p.getId()));
            p.getComments().forEach(c -> c.setTimeAgo(TimeAgo.calculateCommentTimeAgo(c)));
            p.setTimeAgo(TimeAgo.calculatePostTimeAgo(p));});
    }


}
