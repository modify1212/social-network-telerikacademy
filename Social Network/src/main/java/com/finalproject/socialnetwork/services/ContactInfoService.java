package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.ContactInfo;

public interface ContactInfoService {

    ContactInfo getById(int id);

    ContactInfo create(ContactInfo contactInfo);
}
