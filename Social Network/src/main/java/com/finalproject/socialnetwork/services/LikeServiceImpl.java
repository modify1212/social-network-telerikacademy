package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.exceptions.DuplicateEntityException;
import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.repository.LikeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class LikeServiceImpl implements LikeService {
    private LikeRepository likeRepository;


    @Autowired
    public LikeServiceImpl(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    @Override
    public Like create(Like like) {
        if(likeRepository.existsByUserIdAndPostId(like.getUser().getId(), like.getPost().getId())){
            throw new DuplicateEntityException("This post is already liked by that user");
        }
            likeRepository.saveAndFlush(like);
        return like;
    }

    @Override
    public void delete(Like like) {
        if(!likeRepository.existsByUserIdAndPostId(like.getUser().getId(), like.getPost().getId())){
            throw new EntityNotFoundException("This like has already been removed");
        }
        likeRepository.delete(like);
    }

    @Override
    public Like getById(int id) {
        return likeRepository.getOne(id);
    }

    @Override
    public List<Like> getByPost(int postId) {
        return likeRepository.findAllByPostId(postId);
    }

    @Override
    public Like getByUserIdAndPostId(int userId, int postId){
        if(!likeRepository.existsByUserIdAndPostId(userId, postId)){
            throw new EntityNotFoundException("This like does not exist in the DB");
        }
        return likeRepository.getByUserIdAndPostId(userId,postId);
    }
}
