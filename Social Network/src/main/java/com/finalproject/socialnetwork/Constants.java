package com.finalproject.socialnetwork;

public class Constants {

    public static final int DEFAULT_COUNTRY_ID = 247;
    public static final int DEFAULT_TEAM_ID  = 1;
    public static final String DEFAULT_MOBILE = "Not Specified";
    public static final String DEFAULT_BIRTH_YEAR = "Not Specified";
    public static final int MAX_COMMENTS_PER_POST = 5;
    public static final long TIME_DIVISION_FACTOR = 100000;
    public static final int LIKE_MULTIPLICATION_FACTOR = 10;
    public static final int COMMENT_MULTIPLICATION_FACTOR = 10;
}
