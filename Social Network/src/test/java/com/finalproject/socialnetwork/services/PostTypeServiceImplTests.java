package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.PostType;
import com.finalproject.socialnetwork.repository.PostTypeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


import static com.finalproject.socialnetwork.Factory.createPostType;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class PostTypeServiceImplTests {

    @Mock
    PostTypeRepository repository;

    @InjectMocks
    PostTypeServiceImpl mockService;




    @Test
    public void getById_ShouldReturnPostType_WhenPostTypeExists(){
        //Arrange
        PostType expectedPostType =  createPostType();
        //Act
        Mockito.when(repository.getOne(anyInt())).thenReturn(expectedPostType);

        PostType returnedPosts= mockService.getById(anyInt());
        //Assert
        Assert.assertSame(expectedPostType,returnedPosts);
    }

       @Test
    public void getAll_ShouldReturnListOfPosts(){
        //Arrange
        List<PostType> expectedPostTypes =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedPostTypes);
        List<PostType> returnedPosts= mockService.getAll();
        //Assert
        Assert.assertSame(expectedPostTypes,returnedPosts);
    }
}
