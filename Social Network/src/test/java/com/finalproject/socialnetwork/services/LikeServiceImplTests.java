package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.exceptions.DuplicateEntityException;
import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.LikeRepository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.finalproject.socialnetwork.Factory.createLike;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class LikeServiceImplTests {
    @Mock
    LikeRepository repository;

    @InjectMocks
    LikeServiceImpl mockService;


    @Test
    public void create_Should_ReturnLike_WhenSuccessful() {

        //Arrange
        Like expectedLike = createLike();
        Mockito.when(repository.saveAndFlush(expectedLike))
                .thenReturn(expectedLike);

        //Act
        Like returnedLike= mockService.create(expectedLike);

        //Assert
        Assert.assertSame(expectedLike,returnedLike);

    }

    @Test(expected = DuplicateEntityException.class) //Assert
    public void create_ShouldThrow_IfLikeAlreadyExists() {
        //Arrange
        Like like = createLike();

        Mockito.when(repository.existsByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(true);
        //Act
        mockService.create(like);
    }
    @Test
    public void getById_ShouldReturnLike_WhenLikeExists() {
        //Arrange

        Like expectedLike = createLike();

        Mockito.when(repository.getOne(anyInt())).thenReturn(expectedLike);
        //Act
        Like resultLike = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(resultLike, expectedLike );
    }

    @Test
    public void getByPost_ShouldReturnListOfLikes(){
        //Arrange
        List<Like> expectedLikes =  new ArrayList<>();
        Mockito.when(repository.findAllByPostId(anyInt()))
                .thenReturn(expectedLikes);
        //Act
        List<Like> returnedLikes= mockService.getByPost(anyInt());

        //Assert
        Assert.assertSame(expectedLikes,returnedLikes);
    }


    @Test
    public void delete_Should_CallRepository() {
        //Arrange
        Like like = createLike();

        Mockito.when(repository.existsByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(true);
        //Act

        mockService.delete(like);
        //Assert
        Mockito.verify(repository, times(1))
                .delete(like);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void delete_ShouldThrow_IfLikeAlreadyDeleted() {
        //Arrange
        Like like = createLike();

        Mockito.when(repository.existsByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(false);
        //Act
        mockService.delete(like);
    }

    @Test
    public void getByUserIdAndPostId_ShouldReturnLike(){
        //Arrange
        Like expectedLike = createLike();
        Mockito.when(repository.existsByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(true);

        Mockito.when(repository.getByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(expectedLike);

        //Act
        Like returnedLike= mockService.getByUserIdAndPostId(anyInt(),anyInt());

        //Assert
        Assert.assertSame(expectedLike,returnedLike);
    }
    @Test(expected = EntityNotFoundException.class) //Assert
    public void getByUserIdAndPostId_ShouldThrow_IfLikeDoesNotExists() {
        //Arrange
        Like like = createLike();

        Mockito.when(repository.existsByUserIdAndPostId(anyInt(),anyInt()))
                .thenReturn(false);
        //Act
        mockService.getByUserIdAndPostId(anyInt(),anyInt());
    }
}
