package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.PostType;
import com.finalproject.socialnetwork.repository.LikeRepository;
import com.finalproject.socialnetwork.repository.PostRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;


import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.createLike;
import static com.finalproject.socialnetwork.Factory.createPost;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class PostServiceImplTests {
    @Mock
    PostRepository repository;

    @Mock
    LikeService mockLikeService;

    @InjectMocks
    PostServiceImpl mockService;

    Pageable pageable;

    @Test
    public void create_Should_ReturnPost_WhenSuccessful() {

        //Arrange
        Post expectedPost = createPost();

        Mockito.when(repository.saveAndFlush(expectedPost))
                .thenReturn(expectedPost);

        //Act
        Post returnedLike= mockService.create(expectedPost);

        //Assert
        Assert.assertSame(expectedPost,returnedLike);

    }
    @Test
    public void update_ShouldUpdatePost_WhenSuccessful() {
        //Arrange, Act
        Post postToUpdate = createPost();
        mockService.update(postToUpdate);
        //Assert
        Mockito.verify(repository, times(1)).saveAndFlush(postToUpdate);
    }

    @Test
    public void delete_ShouldDeletePost_WhenPassedValidPost() {
        //Arrange, Act
        Post postToDelete = createPost();
        mockService.delete(postToDelete);
        //Assert
        Mockito.verify(repository, times(1)).delete(postToDelete);
    }


    @Test
    public void softDelete_ShouldSoftDeletePost_WhenPassedValidPost() {
        //Arrange, Act
        Post postToDelete = createPost();
        mockService.softDelete(postToDelete);
        //Assert
        Mockito.verify(repository, times(1)).saveAndFlush(postToDelete);
    }

    @Test
    public void getById_ShouldReturnPost_WhenPostExists(){
        //Arrange
        Post expectedPost =  createPost();
        List<Like> expectedLikes= new ArrayList<>();
        //Act
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedPost);
        expectedPost.setLikes(expectedLikes);

        Post returnedPosts= mockService.getById(anyInt());
        //Assert
        Assert.assertSame(expectedPost,returnedPosts);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getByUserIdAndPostId_ShouldThrow_IfLikeDoesNotExists() {
        //Arrange
        Like like = createLike();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);
        //Act
        mockService.getById(anyInt());
    }
    @Test
    public void getFeed_ShouldReturnListOfPosts(){
        //Arrange
        List<Post>posts= new ArrayList<>();
        Page<Post> expectedPosts =  new PageImpl<>(posts);

        //Act
        Mockito.when(repository.getAll(pageable))
                .thenReturn(expectedPosts);

        Page<Post> returnedPosts= mockService.getFeed(pageable);
        //Assert
        Assert.assertSame(expectedPosts,returnedPosts);
    }
//    @Test
//    public  void getByTypeId_ShouldReturnPageOfPosts(){
//        //Arrange
//        List<Post>posts= new ArrayList<>();
//        Post post =createPost();
//        PostType postType= new PostType();
//        postType.setPostType("tech");
//        post.setPostType(postType);
//        posts.add(post);
//        Page<Post> expectedPosts =  new PageImpl<>(posts);
//
//
//        //Act
//        Mockito.when(repository.findAllByPostTypeIdOrderByPopularityDesc(pageable,anyInt()))
//                .thenReturn(expectedPosts);
//
//        Page<Post> returnedPosts= mockService.getByTypeId(pageable,anyInt());
//        //Assert
//        Assert.assertSame(expectedPosts,returnedPosts);
//    }
    @Test
    public void getByUser_ShouldReturnListOfPosts(){
        //Arrange
        List<Post> expectedPosts =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAllByUserIdOrderByIdDesc(anyInt()))
                .thenReturn(expectedPosts);

        List<Post> returnedPosts= mockService.getByUser(anyInt());
        //Assert
        Assert.assertSame(expectedPosts,returnedPosts);
    }

    @Test
    public void getAll_ShouldReturnListOfPosts(){
        //Arrange
        List<Post> expectedPosts =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedPosts);
        List<Post> returnedPosts= mockService.getAll();
        //Assert
        Assert.assertSame(expectedPosts,returnedPosts);
    }
    @Test
    public void updatePopularity_ShouldChangePopularity_WhenCorrectPost() {
        //Arrange,Act
        Post expectedPost= createPost();
       Mockito.when(repository.getById(anyInt())).thenReturn(expectedPost);
       mockService.updatePopularity(expectedPost.getId());
       //Assert
       Mockito.verify(repository,times(1)).saveAndFlush(expectedPost);

    }
}
