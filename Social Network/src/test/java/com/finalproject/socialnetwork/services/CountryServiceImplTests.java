package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Country;
import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.repository.CountryRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.createCountry;
import static com.finalproject.socialnetwork.Factory.createTeam;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CountryServiceImplTests {

    @Mock
    CountryRepository repository;

    @InjectMocks
    CountryServiceImpl mockService;

    @Test
    public void getById_Should_ReturnCountry_WhenCountryExists() {
        //Arrange

        Country expectedCountry = createCountry();

        Mockito.when(repository.getById(anyInt())).thenReturn(expectedCountry);
        //Act
        Country resultCountry = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(resultCountry, expectedCountry );
    }

    @Test
    public void getAll_ShouldReturnListOfCountries(){
        //Arrange
        List<Country> expectedTeams =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedTeams);
        List<Country> returnedCountries= mockService.getAll();
        //Assert
        Assert.assertSame(expectedTeams,returnedCountries);
    }

}
