package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.exceptions.DuplicateEntityException;
import com.finalproject.socialnetwork.models.FriendRequest;
import com.finalproject.socialnetwork.models.Friendship;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.FriendRequestRepository;
import com.finalproject.socialnetwork.repository.FriendshipRepository;
import com.finalproject.socialnetwork.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.createFriendRequest;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class FriendRequestServiceImplTests {

    @Mock
    FriendRequestRepository repository;
    @Mock
    UserRepository mockUserRepository;
    @Mock
    FriendshipRepository mockFriendshipRepository;

    @InjectMocks
    FriendRequestServiceImpl mockService;

    @Test
    public void delete_ShouldDeleteFriendRequest_WhenPassedValidFriendRequest(){
        //Arrange,Act
        FriendRequest friendRequestToDelete=createFriendRequest();
        mockService.delete(friendRequestToDelete);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).delete(friendRequestToDelete);
    }

    @Test
    public void  getByUserId_ShouldReturnListOfFriendRequests(){
        //Arrange
        List<FriendRequest> expectedFriendRequests= new ArrayList<>();

        //Act
        Mockito.when(repository.findByRecipientIdOrderByIdDesc(anyInt()))
                .thenReturn(expectedFriendRequests);

        List<FriendRequest>retunedFriendRequests= mockService.getByUserId(anyInt());
        //Assert
        Assert.assertSame(expectedFriendRequests,retunedFriendRequests);
    }

    @Test
    public void  findRequest_ShouldReturnListOfFriendRequests(){
        //Arrange
        List<FriendRequest> expectedFriendRequests= new ArrayList<>();

        //Act
        Mockito.when(repository.findRequest(anyInt(),anyInt()))
                .thenReturn(expectedFriendRequests);

        List<FriendRequest>retunedFriendRequests= mockService.findRequest(anyInt(),anyInt());
        //Assert
        Assert.assertSame(expectedFriendRequests,retunedFriendRequests);
    }
    @Test
    public void accept_ShouldDeleteFriendRequest () {
        //Arrange

        FriendRequest expectedFriendRequest = new FriendRequest();
        Friendship friendship = new Friendship();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expectedFriendRequest);
        //Act
        mockService.accept(anyInt());

        //Assert
        Mockito.verify(repository,Mockito.times(1)).delete(expectedFriendRequest);
    }


    @Test
    public void reject_ShouldDeleteFriendRequest_WhenPassedValidFriendRequestId(){
        //Arrange,Act
        FriendRequest friendRequestToDelete=createFriendRequest();
        mockService.reject(anyInt());
        //Assert
        Mockito.verify(repository,Mockito.times(1)).delete(repository.getOne(anyInt()));
    }
        @Test(expected = DuplicateEntityException.class) //Assert
    public void send_ShouldThrow_IfFriendRequestAlreadyExists() {
        //Arrange
        FriendRequest friendRequest = new FriendRequest();

        Mockito.when(repository.findBySenderIdAndRecipientId(1,1))
                .thenReturn(friendRequest);
        //Act
        mockService.send(1,1);
    }

    @Test
    public void send_Should_ReturnFriendRequest_WhenUserExists() {
        //Arrange
        FriendRequest expectedFriendRequest= createFriendRequest();

        repository.saveAndFlush(expectedFriendRequest);

        User sender= mockUserRepository.getById(anyInt());
        User recipient= mockUserRepository.getById(anyInt());

        expectedFriendRequest.setSender(sender);
        expectedFriendRequest.setRecipient(recipient);

        //Act
        mockService.send(anyInt(),anyInt());

        //Assert
        Mockito.verify(repository,Mockito.times(1)).saveAndFlush(expectedFriendRequest);



    }
    }
