package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.exceptions.PasswordMismatchException;
import com.finalproject.socialnetwork.exceptions.WrongPasswordException;
import com.finalproject.socialnetwork.models.*;
import com.finalproject.socialnetwork.models.dtos.UserPasswordChangeDTO;
import com.finalproject.socialnetwork.models.mappers.UserMapper;
import com.finalproject.socialnetwork.repository.UserRepository;
import com.finalproject.socialnetwork.utils.UserUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.finalproject.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTests {

    @Mock
    UserRepository mockUsersRepository;
    @Mock
    UserDetailsManager userDetailsManager;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    ContactInfoService mockContactInfoService;
    @Mock
    CountryService mockCountryService;
    @Mock
    TeamService mockTeamService;
    @Mock
    UserUtils mockUserUtils;

    @InjectMocks
    UserServiceImpl mockService;

//
//    @Test
//    public void create_Should_ReturnUser_WhenSuccessful() {
//        //Arrange
//        User user = createUser();
//        UserMapper userMapper = new UserMapper( mockContactInfoService,
//                mockCountryService,
//                mockTeamService,
//                mockUsersRepository,
//                passwordEncoder,
//                userDetailsManager);
//
//        Mockito.when(userMapper.registeredUserToUser(user)).thenReturn(user);
//        //Act
//        User returnedUser = mockService.create(user);
//        //Assert
//        Assert.assertEquals(returnedUser, user);
//    }

    @Test
    public void update_ShouldUpdateUser_WhenSuccessful() {
        //Arrange, Act
        User userToUpdate = createUser();
        mockService.update(userToUpdate);
        //Assert
        Mockito.verify(mockUsersRepository, times(1)).saveAndFlush(userToUpdate);
    }

    @Test
    public void softDelete_ShouldSoftDeleteUser_WhenPassedValidUser() {
        //Arrange, Act
        User userToDelete = createUser();
        mockService.softDelete(userToDelete);
        //Assert
        Mockito.verify(mockUsersRepository, times(1)).saveAndFlush(userToDelete);
    }

    @Test
    public void delete_ShouldDeleteUser_WhenPassedValidUser() {
        //Arrange, Act
        User userToDelete = createUser();
        mockService.delete(userToDelete);
        //Assert
        Mockito.verify(mockUsersRepository, times(1)).delete(userToDelete);
    }

    @Test
    public void getById_ShouldReturnUser_WhenUserExists() {
        //Arrange

        User expectedUser = createUser();

        Mockito.when(mockUsersRepository.getById(anyInt())).thenReturn(expectedUser);
        //Act
        User resultComment = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(resultComment, expectedUser);
    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void getByID_ShouldThrow_WhenIdDoesNotExist() {
        //Arrange
        Mockito.when(mockUsersRepository.getById(anyInt()))
                .thenThrow(new EntityNotFoundException());
        //Act
        mockService.getById(anyInt());
    }

    @Test
    public void getByUserName_ShouldReturnUser_WhenUserWithIdExist() {
        //Arrange

        User expectedUser = createUser();

        Mockito.when(mockUsersRepository.getByUsername(anyString())).thenReturn(expectedUser);
        //Act
        User resultComment = mockService.getByUsername(anyString());

        //Assert
        Assert.assertSame(resultComment, expectedUser);
    }

    @Test
    public void getAll_ShouldReturnListOfComments() {
        //Arrange

        List<User> expectedUsers = new ArrayList<>();

        Mockito.when(mockUsersRepository.findAll()).thenReturn(expectedUsers);
        //Act
        List<User> resultUsers = mockService.getAll();

        //Assert
        Assert.assertSame(resultUsers, expectedUsers);
    }

    @Test
    public void getByTeam_ShouldReturnUser_WhenUserExists() {
        //Arrange

        List<User> expectedUsers = new ArrayList<>();

        Mockito.when(mockUsersRepository.findAllByTeamId(anyInt())).thenReturn(expectedUsers);
        //Act
        List<User> resultUsers = mockService.getByTeam(anyInt());

        //Assert
        Assert.assertSame(expectedUsers, resultUsers);
    }

    @Test
    public void searchUser_ShouldReturnListOfUsers_WhenUsersExists() {
        //Arrange

        List<User> expectedUsers = new ArrayList<>();

        Mockito.when(mockUsersRepository.searchUser(anyString())).thenReturn(expectedUsers);
        //Act
        List<User> resultUsers = mockService.searchUser(anyString());

        //Assert
        Assert.assertSame(expectedUsers, resultUsers);
    }

    @Test
    public void changePassword_ShouldCallRepository_WhenSuccessful() {
        //Arrange
        User expectedUser = createUser();

        Mockito.when(mockUsersRepository.getById(anyInt())).thenReturn(expectedUser);

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();

        Mockito.when(passwordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        expectedUser.setPasswordConfirmation(userPasswordChangeDTO.getNewPassword());

        //Act
        mockService.changePassword(userPasswordChangeDTO);
        //Assert
        Mockito.verify(mockUsersRepository, Mockito.times(1))
                .saveAndFlush(expectedUser);
    }

    @Test(expected = WrongPasswordException.class) //Assert
    public void changePassword_ShouldThrow_WhenPasswordIsWrong() {
        //Arrange
        User expectedUser = createUser();
        Mockito.when(mockUsersRepository.getById(anyInt()))
                .thenReturn(expectedUser);

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();
        userPasswordChangeDTO.setOldPassword(expectedUser.getPassword() + expectedUser.getPassword());
        //Act
        mockService.changePassword(userPasswordChangeDTO);
    }


    @Test(expected = PasswordMismatchException.class) //Assert
    public void changePassword_ShouldThrow_WhenPasswordsDoNotMatch() {
        //Arrange
        User expectedUser = createUser();

        Mockito.when(mockUsersRepository.getById(anyInt()))
                .thenReturn(expectedUser);

        UserPasswordChangeDTO userPasswordChangeDTO = createUserPasswordChangeDTO();
        Mockito.when(passwordEncoder.matches(anyString(), anyString()))
                .thenReturn(true);

        userPasswordChangeDTO.setNewPasswordConfirmation(userPasswordChangeDTO.getNewPassword() + userPasswordChangeDTO.getNewPassword());

        //Act
        mockService.changePassword(userPasswordChangeDTO);
    }


    @Test
    public void leaveTeam_ShouldChangeUsersTeamToDefault_WhenUserWithIdNotExist() {
        //Arrange, Act
        User userToUpdate = createUser();
        Mockito.when(mockUsersRepository.getById(anyInt())).thenReturn(userToUpdate);

        mockService.leaveTeam(1);

        //Assert
        Mockito.verify(mockUsersRepository, Mockito.times(1))
                .saveAndFlush(userToUpdate);

    }

}
