package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.models.ContactInfo;
import com.finalproject.socialnetwork.repository.ContactInfoRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static com.finalproject.socialnetwork.Factory.createComment;
import static com.finalproject.socialnetwork.Factory.createContactInfo;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class ContactInfoServiceImplTests {
    @Mock
    ContactInfoRepository repository;

    @InjectMocks
    ContactInfoServiceImpl mockService;


    @Test
    public void create_ShouldReturnContactInfo_WhenSuccessful() {
        //Arrange
        ContactInfo expectedContactInfo = createContactInfo();

        Mockito.when(repository.saveAndFlush(expectedContactInfo))
                .thenReturn(expectedContactInfo);

        //Act
        ContactInfo returnedContactInfo= mockService.create(expectedContactInfo);

        //Assert
        Assert.assertSame(expectedContactInfo,returnedContactInfo);
    }

    @Test
    public void getById_Should_ReturnComment_WhenCommentExists() {
        //Arrange

        ContactInfo expectedContactInfo = createContactInfo();

        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expectedContactInfo);
        //Act
        ContactInfo returnedContactInfo= mockService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedContactInfo,returnedContactInfo);
    }
}
