package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Comment;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.repository.CommentRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.createComment;
import static com.finalproject.socialnetwork.Factory.createTeam;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;


@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTests {
    @Mock
    CommentRepository repository;

    @InjectMocks
    CommentServiceImpl mockService;


    @Test
    public void create_ShouldReturnComment_WhenSuccessful() {
        //Arrange
        Comment expectedComment = createComment();

        Mockito.when(repository.saveAndFlush(expectedComment))
                .thenReturn(expectedComment);

        //Act
        Comment returnedComment= mockService.create(expectedComment);

        //Assert
        Assert.assertSame(expectedComment,returnedComment);
    }
    @Test
    public void update_ShouldUpdateComment_WhenSuccessful() {
        //Arrange, Act
        Comment commentToUpdate = createComment();
        mockService.update(commentToUpdate);
        //Assert
        Mockito.verify(repository, times(1)).saveAndFlush(commentToUpdate);
    }
    @Test
    public void delete_ShouldDeleteComment_WhenPassedValidComment() {
        //Arrange, Act
        Comment commentToDelete = createComment();
        mockService.delete(commentToDelete);
        //Assert
        Mockito.verify(repository, times(1)).delete(commentToDelete);
    }

//    @Test
//    public void softDelete_ShouldSoftDeleteComment_WhenPassedValidComment() {
//        //Arrange, Act
//        Comment commentToDelete = createComment();
//        mockService.softDelete(commentToDelete);
//        //Assert
//        Mockito.verify(repository, times(1)).saveAndFlush(commentToDelete);
//    }

    @Test
    public void getById_Should_ReturnComment_WhenCommentExists() {
        //Arrange

        Comment expectedComment = createComment();

        Mockito.when(repository.getOne(anyInt())).thenReturn(expectedComment);
        //Act
        Comment resultComment = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(resultComment, expectedComment );
    }

    @Test
    public void getAll_ShouldReturnListOfComments(){
        //Arrange
        List<Comment> expectedComments =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedComments);
        List<Comment> returnedComment= mockService.getAll();
        //Assert
        Assert.assertSame(expectedComments,returnedComment);
    }

    @Test
    public void getByPost_ShouldReturnListOfComments(){
        //Arrange
        List<Comment> expectedComments =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAllByPostId(anyInt()))
                .thenReturn(expectedComments);

        List<Comment> returnedComments= mockService.getByPost(anyInt());
        //Assert
        Assert.assertSame(expectedComments,returnedComments);
    }

    @Test
    public void getByUser_ShouldReturnListOfComments(){
        //Arrange
        List<Comment> expectedComments =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAllByUserId(anyInt()))
                .thenReturn(expectedComments);

        List<Comment> returnedComments= mockService.getByUser(anyInt());
        //Assert
        Assert.assertSame(expectedComments,returnedComments);
    }

}
