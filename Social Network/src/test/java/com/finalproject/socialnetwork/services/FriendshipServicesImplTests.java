package com.finalproject.socialnetwork.services;


import com.finalproject.socialnetwork.models.Friendship;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.FriendshipRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;


import static com.finalproject.socialnetwork.Factory.createFriendship;
import static com.finalproject.socialnetwork.Factory.createUser;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class FriendshipServicesImplTests {


    @Mock
    FriendshipRepository repository;

    @InjectMocks
    FriendshipServiceImpl mockService;


    @Test
    public void getFriendsByUserId_ShouldReturnListOfFriendships() {
        //Arrange

        User friendOne = createUser();
        User friendTwo = createUser();
        Friendship newFriendship = createFriendship();

        List<Friendship> friendships = new ArrayList<>();

        List<User> expectedFriends = new ArrayList<>();
        newFriendship.setFriendOne(friendOne);
        newFriendship.setFriendTwo(friendTwo);
        friendships.add(newFriendship);

        Mockito.when(repository.findAllByFriendTwoId(1)).thenReturn((friendships));
        Mockito.when(repository.findAllByFriendOneId(1)).thenReturn((friendships));
        expectedFriends.add(newFriendship.getFriendTwo());
        expectedFriends.add(newFriendship.getFriendOne());


        //Act
        List<User> returnedFriendships = mockService.getFriendsByUserId(1);

        //Assert
        Assert.assertEquals(expectedFriends,returnedFriendships);
    }


    @Test
    public void unFriend_ShouldDeleteFriendship() {
        //Arrange, Act
        List<Friendship> friendships = new ArrayList<>();
        Friendship friendship= createFriendship();
        friendships.add(friendship);
        Mockito.when(repository.findFriendship(anyInt(),anyInt())).thenReturn(friendships);
        mockService.unFriend(anyInt(),anyInt());
        //Assert
        Mockito.verify(repository, Mockito.times(1)).delete(friendships.get(0));

    }

    @Test(expected = EntityNotFoundException.class) //Assert
    public void unFriend_Should_Throw_WhenFriendshipDoesNotExist() {
        //Arrange
        Mockito.when(repository.findFriendship(anyInt(),anyInt()))
                .thenThrow(new EntityNotFoundException());
        //Act
        repository.findFriendship(anyInt(),anyInt());
    }
    }
