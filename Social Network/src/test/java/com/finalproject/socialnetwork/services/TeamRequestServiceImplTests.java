package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.models.TeamRequest;
import com.finalproject.socialnetwork.models.User;
import com.finalproject.socialnetwork.repository.TeamRequestRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class TeamRequestServiceImplTests {

    @Mock
    TeamRequestRepository repository;
    @Mock
    TeamService mockTeamService;
    @Mock
    UserService mockUserService;

    @InjectMocks
    TeamRequestServiceImpl mockService;

    @Test
    public void getAll_ShouldReturnListOfTeamRequests() {

        //Arrange
        List<TeamRequest> expectedTeamRequests = new ArrayList<>();

        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedTeamRequests);

        List<TeamRequest> returnedTeamRequests = mockService.getAll();

        //Assert
        Assert.assertSame(expectedTeamRequests, returnedTeamRequests);

    }

    @Test
    public void getById_ShouldReturnTeamRequest_WhenTeamRequestExists() {

        //Arrange
        TeamRequest expectedTeamRequest = createTeamRequest();

        //Act
        Mockito.when(repository.getOne(anyInt()))
                .thenReturn(expectedTeamRequest);

        TeamRequest returnedTeamRequest = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(expectedTeamRequest, returnedTeamRequest);
    }

    @Test
    public void reject_ShouldDeleteTeamRequest_WhenTeamRequestExists() {

        //Arrange, Act
        TeamRequest expectedTeamRequest = createTeamRequest();

        mockService.reject(anyInt());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).delete(repository.getOne(anyInt()));
    }

    @Test
    public void getByTeam_ShouldReturnListOfTeamRequests() {

        //Arrange
        List<TeamRequest> expectedTeamRequests = new ArrayList<>();

        //Act
        Mockito.when(repository.findByTeamId(anyInt()))
                .thenReturn(expectedTeamRequests);

        List<TeamRequest> returnedTeamRequests = mockService.getByTeam(anyInt());

        //Assert
        Assert.assertSame(expectedTeamRequests, returnedTeamRequests);

    }

//    @Test
//    public void accept_ShouldDeleteTeamRequest_WhenTeamRequestExists() {
//
//        //Arrange
//        TeamRequest expectedTeamRequest = createTeamRequest();
//        User user =createUser();
//        Team team = createTeam();
//        expectedTeamRequest.setUser(user);
//
//        expectedTeamRequest.getUser().setTeam(team);
//
//        //Act
//        mockService.accept(anyInt());
//
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(repository.getOne(1));
//
//    }

    @Test
    public void send_ShouldSaveTeamRequest_WhenTeamRequestExists() {

        //Arrange, Act
        TeamRequest expectedTeamRequest = new TeamRequest();

        repository.saveAndFlush(expectedTeamRequest);

        Team team = mockTeamService.getById(1);
        User user = mockUserService.getById(1);

        expectedTeamRequest.setUser(user);
        expectedTeamRequest.setTeam(team);

        mockService.send(1, 1);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).saveAndFlush(expectedTeamRequest);
    }


    @Test
    public void delete_ShouldDeleteTeamRequest_WhenTeamRequestExists() {

        //Arrange,Act
        TeamRequest expectedTeamRequest = createTeamRequest();

        mockService.delete(anyInt());

        //Assert
        Mockito.verify(repository, Mockito.times(1)).delete(repository.getOne(anyInt()));
    }
}

