package com.finalproject.socialnetwork.services;

import com.finalproject.socialnetwork.models.Like;
import com.finalproject.socialnetwork.models.Post;
import com.finalproject.socialnetwork.models.Team;
import com.finalproject.socialnetwork.repository.TeamRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

import static com.finalproject.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TeamServiceImplTests {
    @Mock
    TeamRepository repository;

    @InjectMocks
    TeamServiceImpl mockService;


    @Test
    public void create_ShouldReturnTeam_WhenSuccessful() {
        //Arrange
        Team expectedTeam = createTeam();

        Mockito.when(repository.saveAndFlush(expectedTeam))
                .thenReturn(expectedTeam);

        //Act
        Team returnedTeam= mockService.create(expectedTeam);

        //Assert
        Assert.assertSame(expectedTeam,returnedTeam);
    }

    @Test
    public void update_ShouldUpdateTeam_WhenSuccessful() {
        //Arrange, Act
        Team teamToUpdate = createTeam();
        mockService.update(teamToUpdate);
        //Assert
        Mockito.verify(repository, times(1)).saveAndFlush(teamToUpdate);
    }

    @Test
    public void delete_ShouldDeleteTeam_WhenPassedValidTeam() {
        //Arrange, Act
        Team teamToDelete = createTeam();
        mockService.delete(teamToDelete);
        //Assert
        Mockito.verify(repository, times(1)).delete(teamToDelete);
    }

    @Test
    public void softDelete_ShouldSoftDeleteTeam_WhenPassedValidPost() {
        //Arrange, Act
        Team teamToDelete = createTeam();
        mockService.softDelete(teamToDelete);
        //Assert
        Mockito.verify(repository, times(1)).saveAndFlush(teamToDelete);
    }
    @Test
    public void getById_Should_ReturnTeam_WhenTeamExists() {
        //Arrange

        Team expectedTeam = createTeam();

        Mockito.when(repository.getById(anyInt())).thenReturn(expectedTeam);
        //Act
        Team resultTeam = mockService.getById(anyInt());

        //Assert
        Assert.assertSame(resultTeam, expectedTeam );
    }

    @Test
    public void getAll_ShouldReturnListOfTeams(){
        //Arrange
        List<Team> expectedTeams =  new ArrayList<>();
        //Act
        Mockito.when(repository.findAll())
                .thenReturn(expectedTeams);
        List<Team> returnedPosts= mockService.getAll();
        //Assert
        Assert.assertSame(expectedTeams,returnedPosts);
    }



}
