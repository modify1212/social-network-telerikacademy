package com.finalproject.socialnetwork;

import com.finalproject.socialnetwork.models.*;
import com.finalproject.socialnetwork.models.dtos.UserPasswordChangeDTO;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import javax.swing.plaf.IconUIResource;
import javax.validation.constraints.NotNull;

import java.util.List;

import static com.finalproject.socialnetwork.Constants.DEFAULT_MOBILE;

public class Factory {

    public static Like createLike() {
        User user = new User();
        Post post = new Post();
        Like like = new Like();
        like.setId(1);
        like.setUser(user);
        like.setPost(post);
        return like;
    }

    public static Post createPost() {
        Post post = new Post();
        post.setId(1);
        post.setDescription("Description");
        return post;

    }
    public static PostType createPostType() {
        PostType postType = new PostType();
        return postType;

    }
    public static ContactInfo createContactInfo() {
        ContactInfo contactInfo = new ContactInfo();
        return contactInfo;

    }
    public static  User createUser(){

        PasswordEncoder passwordEncoder;
        UserDetailsManager userDetailsManager;
        User user = new User();
        Team team = new Team();
        ContactInfo contactInfo=new ContactInfo();
        team.setId(1);
        Country country=new Country();
        user.setId(1);
        user.setPassword("password123");
        user.setPasswordConfirmation("password123");

        user.setFirstName("Ivan");
        user.setLastName("Ivan");
        user.setUsername("ivan.pppppp@gmail.com");
        contactInfo.setEmail(user.getUsername());
        contactInfo.setMobile(DEFAULT_MOBILE);
        contactInfo.setPrivacy(1);
        user.setContactInfo(contactInfo);
        user.setBirthYear("1990");
        user.setCountry(country);
        user.setTeam(team);
        return user;
    }

    public static UserPasswordChangeDTO createUserPasswordChangeDTO() {
        UserPasswordChangeDTO userPasswordChangeDTO = new UserPasswordChangeDTO();

        userPasswordChangeDTO.setId(1);
        userPasswordChangeDTO.setOldPassword("oldPass");
        userPasswordChangeDTO.setNewPassword("newPass");
        userPasswordChangeDTO.setNewPasswordConfirmation("newPass");

        return userPasswordChangeDTO;
    }

    public static  FriendRequest createFriendRequest(){
        FriendRequest friendRequest= new FriendRequest();
        return friendRequest;
    }
    public static  Friendship createFriendship(){
        Friendship friendship= new Friendship();
        return friendship;
    }
    public static Comment createComment(){
        Comment comment = new Comment();
        User user = new User();
        Post post = new Post();
        comment.setId(1);
        comment.setPost(post);
        comment.setUser(user);
        return comment;
    }

    public static Team createTeam() {
        Team team = new Team();
        team.setId(1);
        return team;
    }
    public static TeamRequest createTeamRequest(){
        TeamRequest teamRequest = new TeamRequest();
        return teamRequest;
    }

    public static Country createCountry(){
        Country country= new Country();
        return country;
    }
}
